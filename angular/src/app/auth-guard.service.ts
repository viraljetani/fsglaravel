declare var localStorage : any;
import { Injectable }     from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot }    from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const myToken = localStorage.getItem('token'); // check localStorage
    if (myToken) {
      return true;
    }
	else{
		this.router.navigate(['auth/login'], { queryParams: { returnUrl: state.url }});
		return false;
	}
  }
}