declare var localStorage : any;
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Http, Headers, Response } from '@angular/http';
import { Router  } from "@angular/router";
declare var $:any;
@Component({
    moduleId:module.id,
    selector: 'login-cmp',
    templateUrl: './login.component.html'
})

export class LoginComponent implements OnInit{
    test : Date = new Date();
	loginMessage="Please Login Here";
	constructor(public fb: FormBuilder, private http:Http, private router: Router) {
		if(localStorage.getItem('token')!=null){
			//Already Loggedin redirect to dashboard
			//alert(localStorage.getItem('token'));
			this.loginMessage='Already Loggedin';
			this.router.navigate(['dashboard']);
		}else{
			//Login Now
		}
	}
	checkFullPageBackgroundImage(){
        var $page = $('.full-page');
        var image_src = $page.data('image');

        if(image_src !== undefined){
            var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
            $page.append(image_container);
        }
    };
    ngOnInit(){
        this.checkFullPageBackgroundImage();
		localStorage.getItem('token');
		setTimeout(function(){
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    }
	public loginForm = this.fb.group({
		email: ["", Validators.required],
		password: ["", Validators.required]
	});
	doLogin(event) {
		let headers = new Headers();
		
		//headers.append('Content-Type', 'application/x-www-form-urlencoded');
		headers.append('Access-Control-Allow-Origin', 'http://192.168.1.5');
		headers.append('Access-Control-Allow-Credentials', 'true');
		var data = this.loginForm.value;
		this.loginMessage='Checking...';
		this.http.post('http://192.168.1.5/lumen/public/api/auth/login',data,{headers}).subscribe(
		(response)=>{ this.loginMessage='Success'; 
				if(response.status === 200){
					var result = response.json();
					var role = result.userRole[0].name;
					var token = result.apiResponse.token;
					localStorage.setItem('token',token);
					localStorage.setItem('role',role);
					this.loginMessage='Success';
					this.router.navigate(['dashboard']);
				}
				else if(response.status === 210){
					this.loginMessage = 'Invalid Credentials';
				}
				else{
					this.loginMessage = 'Required Credentials';
				}
			}, 
			(err)=>{ 
				console.log(err) 
			});
  }
}
