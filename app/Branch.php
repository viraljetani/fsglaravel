<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;


class Branch extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
        use Authenticatable, Authorizable;

	    protected $table='branch';

       protected $fillable = [
                    'branchName',
       							'userId',
       							'companyId',
       						];


       	  public function Branch_id($userID,$id)
     {
            $branch_id=DB::table('branch')->where('userID',$userID & 'id',$id)->select('id')->get(); 
            return $branch_id;
      }
    
         public function BranchesDetail($userID )
     {
            $branch1=DB::table('branch')->where('userId',$userID)->select('*')->get(); 
    
        return $branch1;
    }
        public function BranchesDetailAll($userID )
     {
            $branch1=DB::table('branch')->select('*')->get(); 
    
        return $branch1;
    }
}
