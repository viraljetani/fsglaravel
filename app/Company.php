<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Company extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table='company';

       protected $fillable = ['compName',
        'compShortName',
		'rfc',
        'crup',
        'firstName',
        'lastName',
        'email',
        'kind',
        'key',
        'document',
        'street',
        'outdoorNo',
        'interiorNo',
        'colony',
        'city',
        'cp',
        'country',
        'state',
        'nationality',
        'startDate',
        'paymentPeriod',
        'contractedDevices',
        'contractedOperator',
        'userID',
           ];
        
  

     
       public function CompaniesDetail($userID)
     {
            $company1=DB::table('company')->where('userID',$userID )->select('*')->get(); 
                                    
    
        return $company1;
    }
     public function CompaniesDetail1($userID)
     {
            $company11=DB::table('company')->select('*')->get(); 
                                    
    
        return $company11;
    }
   public function CompanyDetail($id)
     {
            $comp_id=DB::table('company')->where('userID',$id)->select('id')->get(); 
            return $comp_id;
    
    }
        

}
