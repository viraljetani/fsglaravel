<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Customer extends Model implements
 	   AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table='customer';

       protected $fillable =[
       						'firstName',
       						'lastName',
       						'middleName',
       						'userId',

       					];
         public function CustomerDetail($id)
     {
            $customer=DB::table('customer')->where('userID',$id)->select('*')->get(); 
    
        return $customer;
    }


 }

