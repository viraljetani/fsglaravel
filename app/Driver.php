<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Driver extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table='drivers';

       protected $fillable =[
       						'firstName',
       						'lastName',
       						'middleName',
       						'photoOfChofer',
       						'licensePicture',
       						'licenseExpDate',
       						'typeLicense',
       						'licenseNum',
       						'rfc',
       						'curp',
       						'socialSecurityNo',
       						'birthDate',
       						'placeOfBirth',
       						'street',
       						'outdoorNo',
       						'interiorNo',
       						'colony',
       						'city',
       						'cp',
       						'country',
       						'state',
       						'nationality',
                  'action',
       				     'companyID',
              		'userID',
                  'branchID'
       				
       						];
         public function DriverDetail($id)
     {
            $drivers=DB::table('drivers')->where('userID',$id)->select('*')->get(); 
    
        return $drivers;
    }


 }


