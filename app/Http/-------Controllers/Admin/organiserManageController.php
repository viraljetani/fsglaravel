<?php

namespace App\Http\Controllers\Admin;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Models\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\User;
use App\Models\Admin\OrganiserManage;
use App\Models\Admin\roleManage;
use Session;

class organiserManageController extends Controller
{
    	protected function validator(array $data)
        {
            return Validator::make($data,
            [
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'conform_password' => 'required|same:password',
            ]);
        }

        //show all organiser controller
    	public function index()
    	{
			$post = User::where('role_data','2')->get();
			return view('pages.admin.pages.organiser',compact('post'));
    	}

    	public function newOrganiser()
    	{
    		return view('pages.admin.pages.newOrganiser');
    	}

        //save new Organiser Controller
        public function saveOrganiser(Request $request)
        {
            $data = $request->all();
            $post = new organiserManage;
            $post->name=$data['name'];
            $post->first_name=$data['first_name'];
            $post->last_name=$data['last_name'];
            $post->email=$data['email'];
            $post->mobile=$data['mobile'];
            $post->noManagerCreate = $data['noManagerCreate'];
            $post->noParkingCreate = $data['noParkingCreate'];
            $post->password=bcrypt($data['password']);
            $post->role_data=$data['role_id'];
            $post->user_role_id=Auth::id();
            $post->activated=1;
            $post->save();
            $insertedId = $post->id;

            $user = new roleManage;
            $user->role_id=$data['role_id'];
            $user->user_id=$insertedId;
            $user->save();
            Session::flash('message', 'Organiser data Saved.');
            Session::flash('alert-class', 'alert-success');
            return redirect('organiser');
        }
        public function showEditOrganiser($id)
        {
             $post = User::find($id);
             return view('pages.admin.pages.editOrganiser',compact('post'));
        }
        public function editOrganiser(Request $request)
        {
             $post = array();
             $post['name'] = $request->input('name');
             $post['first_name'] = $request->input('first_name');
             $post['last_name'] = $request->input('last_name');
             $post['email'] = $request->input('email');
             $post['mobile'] = $request->input('mobile');
             $post['noManagerCreate'] = $request->input('noManagerCreate');
             $post['noParkingCreate'] = $request->input('noParkingCreate');
             $pages = User::where('id',$request->input('id'))->update($post);
             Session::flash('message', 'Organiser data Updated.');
             Session::flash('alert-class', 'alert-success');
             return redirect('organiser');
        }
        public function deleteOrganiser($id)
        {
             $post = User::find($id);
             $post->delete();
             $role = roleManage::where('user_id',$id);    
             $role->delete();
             Session::flash('message', 'Organiser data Deleted.');
             Session::flash('alert-class', 'alert-success');
             return redirect('organiser');
        }
    	    
}
