<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers\Organiser;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Models\Model;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\owner\ParkingModel;
use App\Models\owner\AssignParkingModel;
use App\Models\Admin\ParkingTypesModel;
use Auth;
use Session;
class ParkingController extends Controller
{
    public function index()
    {
        $post = ParkingModel::where('organiser_id',Auth::id())->get();
		return view('pages.owner.pages.parking',compact('post'));
    }
    public function createNewParking()
    {
        $getCreatedManager = User::select('noParkingCreate')
        ->where('id',Auth::id())->get();
        $count = ParkingModel::where('organiser_id',Auth::id())
        ->count();
        
        if($count < $getCreatedManager[0]->noParkingCreate){

        	$post = User::where('role_data','3')->get();
            $row = ParkingTypesModel::where('organiser_id',Auth::id())->get();
            return view('pages.owner.pages.newParking',compact('post','row'));
        }
        else{
            
            Session::flash('message', 'Create Parking limits has been Over');
            Session::flash('alert-class', 'alert-danger');
            return redirect()->back();
        }

    }
    public function saveParking(Request $request)
    {
        $data = $request->all();
        $post = new ParkingModel;
        $post->name=$data['name'];
        $post->city=$data['city'];
        $post->location=$data['location'];
        $post->parking_type=$data['parkingType'];
        $post->organiser_id=Auth::id();
        $post->save();

        $user =new AssignParkingModel;
        $user->parking_id = $post->id;
        $user->organiser_id=Auth::id();
        $user->save();
        Session::flash('message', 'Parking Details saved Sucessfully');
        Session::flash('alert-class', 'alert-success');
        return redirect('parking');
        
    }
    public function showEditParking($id)
    {
        $post = ParkingModel::find($id);
        $row = ParkingTypesModel::where('organiser_id',Auth::id())->get();
        return view('pages.owner.pages.editParking',compact('post','row'));
    }
    public function editParking(Request $request)
    {
        $post = array();
        $post['name'] = $request->input('name');
        $post['city'] = $request->input('city');
        $post['location'] = $request->input('location');
        $post['parking_type'] = $request->input('parkingType');
        $pages = ParkingModel::where('id',$request->input('id'))->update($post);
        Session::flash('message', 'Parking Details Updated Sucessfully');
        Session::flash('alert-class', 'alert-success');
        return redirect('parking');
    }
    public function deleteParking($id)
    {
        $post = ParkingModel::find($id);
        $post->delete();
        $row = AssignParkingModel::where('parking_id',$id);
        $row->delete();
        Session::flash('message', 'Parking Details Deleted Sucessfully');
        Session::flash('alert-class', 'alert-success');
        return redirect('parking');
    }
    public function parkingAssign()
    {
    	$post = User::where('role_data',3)
    	->where('user_role_id',Auth::id())
    	->get();
    	$row = ParkingModel::where('organiser_id',Auth::id())->get();
    	return view('pages.owner.pages.parkingAssign',compact('post','row'));
	}
	public function saveAssignParking(Request $request)
	{	
        $data = $request->all();
        $post = new AssignParkingModel;
        $temp = 0;
        $row = AssignParkingModel::where('parking_id',$data['parking'])->get();
        foreach ($row as $key) 
        {
            if($key->parking_id == $data['parking'])
            {
                $temp = 1;
                $manager_id = explode(",",$key->manager_id);
                $margeManagerId = array_merge($manager_id,$data['manager']);
                $mngr = implode(",",$margeManagerId);
                $data1 = array();
                $data1['manager_id'] = $mngr;
                $pages = AssignParkingModel::where('parking_id',$data['parking'])->update($data1);
            }

        }
        Session::flash('message', 'Parking Manager Assign Sucessfully');
        Session::flash('alert-class', 'alert-success');
        return redirect('parking');
	}
    //ajax function for onchange parkingAssign
	public function addManager($id)
	{	
        $row = User::select('id')->where('role_data',3)
        ->where('user_role_id',Auth::id())->pluck('id')->toArray();
        
        $post = AssignParkingModel::where('parking_id',$id)->get();

        if ($post->count())
        {
           $manager_ids=explode(",",$post[0]->manager_id);
            print_r("<p>Select multiple manager press ctrl and click</p>
                    <select name='manager[]' multiple class='form-control'> 
                    <option>select multiple manager</option>");
          
            for ($i=0; $i < count($row); $i++) { 
                      
                      if(!in_array($row[$i], $manager_ids)){        
                           print_r('<option value='.$row[$i].' id="option" >'.ParkingController::getName($row[$i]).' </option>');
                      }
                  }
            print_r('</select>');           
        }
    }
    public function showManagerParking()
    {
        $post = AssignParkingModel::select('manager_id','parking_id')->get();
        $ary = array();
        foreach ($post as $posts) {
            $manager_ids=explode(",",$posts->manager_id);

            if(in_array(Auth::id(), $manager_ids)){

                $row = ParkingModel::where('id',$posts->parking_id)->get();
                $ary[]=
                array(
                'name' => $row[0]->name,
                'city' => $row[0]->city,
                'location' => $row[0]->location);
            }
        }
        return view('pages.manager.pages.showAssignParking',compact('ary'));
    }
    //function for "User" Table name.
    //function call - ParkingController::getName($row[$i]).
    public static function getName($id)
    {
        $post = User::find($id);
        return $post->name;
    }

}

