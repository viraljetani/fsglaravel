<?php

namespace App\Http\Controllers\Auth;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;
class AuthController extends Controller
{
    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        try {
                $this->validate($request, [
                    'email' => 'required|email|max:255',
                    'password' => 'required',
                ]);
            } 
            catch (ValidationException $e) 
            {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                // 'message' => 'Some Fields are Missing'
                    
                 'message'=>$e->getResponse()    
                         ], ],211);
            }		
        try {
                // Attempt to verify the credentials and create a token for the user
                if (!$token = JWTAuth::attempt( $this->getCredentials($request))) 
                {
                    return $this->onUnauthorized();
                 }
            }
        catch (JWTException $e) {

                    // Something went wrong whilst attempting to encode the token
                    return $this->onJwtGenerationError();
            }
       // All good so return the token
        return $this->onAuthorized($token);
    }
    /**
     * What response should be returned on invalid credentials.
     *
     * @return JsonResponse
     */
    public function postUser(Request $request)
    {
        try{

            $this->validate($request, [
                'name' =>'required',
                'email' => 'required|email|unique:users|max:255',
                'password' => 'required',
                'role'=>'required|max:1'
                                     ]);
            }
            catch (ValidationException $e) 
            {   
          
             return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => $e->getResponse()
                            ], 
                            // 'exception'=>$e->getResponse()
            ],211);
          }
           try{
                $user = User::create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'password'=>Hash::make($request->get('password')),
                           ]);
            }
            catch(Exception $e)
            {
                return $e;
            }
                $asignrole=DB::table('asignrole')->insert(['userId' => 
                    $user->id,'roleID'=>$request->get('role')]);
                            
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The user has been Created' 
                        ],   ]);
            }
    public function send()
    {
        $id=DB::table('userroles')->get();
            return new JsonResponse([
               'message' => 'User Roles',
                'data'=> $userId
                ]);
    }
    protected function onUnauthorized()
    {
        return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => 'Invalid Credentials'
                               ]
                       ],210);
    }
    /**
     * What response should be returned on error while generate JWT.
     *
     * @return JsonResponse
     */
    protected function onJwtGenerationError()
    {
        return new JsonResponse([
            'message' => 'Could Not Create Token'
              ], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * What response should be returned on authorized.
     *
     * @return JsonResponse
     */
    public function onAuthorized($token)
    {   
     $data = JWTAuth::authenticate($token);
        $id = $data->id;
          $obj = new User();
            $role = $obj->getRole($id);
            return new JsonResponse([
            'apiResponse' =>[
                'error'=>false,
                'message'=>'Login Successfully.',

                   'token' => $token
                     ]       ,
                    'userProfile'=>
                                    $data
                                    ,
                    'userRole'=>$role
                ]);
    }
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }
    /**
     * Invalidate a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteInvalidate()
    {
        $token = JWTAuth::parseToken();

        $token->invalidate();

        return new JsonResponse(['message' => 'token_invalidated']);
    }
    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\Response
     */
    public function patchRefresh()
    {
        $token = JWTAuth::parseToken();

        $newToken = $token->refresh();

        return new JsonResponse([
            'message' => 'token_refreshed',
            'data' => [
                'token' => $newToken
                    ]
        ]);
    }
    /**
     * Get authenticated user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUser()
    {   
        $data = JWTAuth::parseToken()->authenticate();
       	$id = $data->id;
		$obj = new User();
		$role = $obj->getRole($id);
	       return new JsonResponse([
                'apiResponse' =>[
                'error'=>false,
                'message' => 'authenticated_user'
                                ],
             'userProfile' => $data,
			 'role' => $role
         ]); 
    }
public function userChange(Request $request)
{
       $data = JWTAuth::parseToken()->authenticate();
        $id = $data->id;
        $obj = new User();
        $role = $obj->getRole($id);
        if(!$data){
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The user with {$id} does not exist' 
                        ],   ]);
        }
        try{
            $this->validate($request, [
                'name' =>'required',
                'email' => 'required|email|max:255',
                'oldpassword'=> 'required',
                'password' => 'required|same:password',
                'role'=>'required|max:1'
                                     ]);
            }
            catch (ValidationException $e) 
            {
             return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => $e->getResponse()
                            ],
	            ],211);
            }
             $data->id =$id;
             $data->name         = $request->get('name');        
             $data->email        = $request->get('email');
             $data->password     = Hash::make($request->get('password'));
      
	        $data->save();
            $asignrole=DB::table('asignrole')->join('userroles','userroles.id','=','asignrole.roleId')->where('asignrole.userId', $id)->UPDATE(['userId' => 
                     $id,'roleID'=>$request->get('role')]);   
                     return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The user has been Updated' 
                        ],   ]);
    }
 }
