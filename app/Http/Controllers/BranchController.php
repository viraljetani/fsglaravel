  <?php

namespace App\Http\Controllers;
use App\Company;
use App\Branch;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;

class BranchController extends Controller
{
   public function postBranch(Request $request)
   {
   		   $user = JWTAuth::parseToken()->authenticate();
  	   	 $userID=$user->id;
         $user = new User();
         $role = $user->getRole($userID);
           foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
      if($roleName=='client')  
        {
            		$obj = new Company();
            		$companyID= $obj->CompaniesDetail($userID);
            	  foreach ($companyID as $compID)
                 {
                   $compID1=$compID->id;
                  } 
        try{

                      $this->validate($request, [
                      'branchName' => 'required',
    	    						        ]);
            }
        catch (ValidationException $e) 
            {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => $e->getResponse()  
                            ],
                         ], 211);
            }
         try{
                          $Branch =Branch::create([
               		       	'branchName'=> $request->get('branchName'),
                          'userId'=>$userID,
                     	    'companyId'=>$compID1,
                           ]);
            }
            catch(Exception $e)
            {
                          return $e;
            }
                return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'The Branch has been Created' 
                            ],   ]);
        }
      else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not register' 
                        ],   ]);
        }
    }
  public function getBranches()
     {
            $user = JWTAuth::parseToken()->authenticate();
            $userID=$user->id;
            $obj = new Branch();
            $obj1 = new Company();    
            $branch=$obj->BranchesDetail($userID);
            $comp = $obj1->CompaniesDetail($userID);
               return new JsonResponse([
                        'apiResponse' =>[
                        'error'=>false
                                ],
                       'message' => 'authenticated_user',
                       'userProfile' => $user,
                       'companyDetail'=>$comp,
                       'branchDetail'=>$branch,
                        ]); 
     }
public function getBranch($id)
    {
           $brid=Branch::find($id);
            if (!$brid) {
                          return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false,
                          'message' => 'Branch does not found' 
                                ],   ]);             
              }
              else
              {
                  $user = JWTAuth::parseToken()->authenticate();
                  $userID=$user->id;
                  $obj = new Branch();
                  $obj1 = new Company();    
                  $branch=$obj->BranchesDetail($userID)->where('id',$id);
                  $comp = $obj1->CompaniesDetail($userID);
                   return new JsonResponse([
                            'apiResponse' =>[
                            'error'=>false
                                    ],
                           'message' => 'authenticated_user',
                           'userProfile' => $user,
                           'companyDetail'=>$comp,
                           'branchDetail'=>$branch,
                            ]); 
           }
    }
public function putBranch(Request $request,$id)
   {
     	    $user = JWTAuth::parseToken()->authenticate();
	   	    $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
                {
                  $roleName=$role1->name;
                }
      if($roleName=='client')  
         {
                 $brid=Branch::find($id);
                    if (!$brid) 
                      {
                          return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false,
                          'message' => 'Branch does not found' 
                                ],   ]);              
                       }
		          $obj = new Company();
              $companyID= $obj->CompaniesDetail($userID);
      	           foreach ($companyID as $company)
            	     	{
      	            		$compID1=$company->id;
      	      	    }
        try{

                  $this->validate($request, [
	    						  'branchName'=>'required',
                         ]);
               }
            catch (ValidationException $e) 
            {
                 return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => 'Some Fields are Missing'
                             ],
                 'exception'=>$e->getResponse()
            ],211);
            }	   		
                  DB::table('branch')->where('id',$id)->where('userID',$userID)->update([  
                    'branchName'=>$request->get('branchName'),
                    'userId'=>$userID, 
                    // 'companyId'=>$compID1,
                        ]);
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Branch has been updated' 
                        ],   ]);
        }
      else
            {
                    return new JsonResponse([
                    'apiResponse' =>[
                    'error'=>false,
                    'message' => 'you can not Update ' 
                          ],   ]);
            }
    }
public function deleteBranch($id)
     {
         	$user = JWTAuth::parseToken()->authenticate();
		      $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
                {
                  $roleName=$role1->name;
                }
        if($roleName=='client')  
         {
                    $brid=Branch::find($id);
                        if (!$brid) 
                        {
                          return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false,
                          'message' => 'Branch does not found' 
                                ],   ]);              
                        }
    	     	    $Delete=DB::table('branch')->where('id',$id)->where('userId',$userID)->delete();
                {

                $table1=DB::table('users')->where('id',$userID)->delete();
                }
          
            	return new JsonResponse([
                  'apiResponse' =>[
                    'error'=>false,
                     'message' => 'Branch  id  deleted' 
                					  ] ]);		
         }
         else
         {
               return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'you can not Delete' 
                            ],   ]);
         }
    }
}