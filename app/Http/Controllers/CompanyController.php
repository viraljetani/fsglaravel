<?php

namespace App\Http\Controllers;
use App\User;
use App\Company;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;

class CompanyController extends Controller
{
   public function postCompany(Request $request)
   {
         		$user = JWTAuth::parseToken()->authenticate();
      		  $id=$user->id;
      		  $obj = new User();
            $role = $obj->getRole($id);
                foreach ($role as $role1)
                   {
                      $roleName=$role1->name;
                    }
        if($roleName=='admin')  
        {
		       	try{
    	        $this->validate($request, [
	                'compName'=>'required',
    			        'compShortName'=>'required',
    					     'rfc'=>'required',
    			        'crup'=>'required',
    			        'firstName'=>'required',
    			        'lastName'=>'required',
    			        'email'=>'required|email|unique:company|max:255',
    			        'kind'=>'required',
    			        'key'=>'required',
    			        'document'=>'required',
    			        'street'=>'required',
    			        'outdoorNo'=>'required|max:12',
    			        'interiorNo'=>'required|max:12',
    			        'colony'=>'required',
    			        'city'=>'required',
    			        'cp'=>'required',
    			       	'country'=>'required',
    			        'state'=>'required',
    			        'nationality'=>'required',
    			        'startDate'=>'required',
    			        'paymentPeriod'=>'required',
    			        'contractedDevices'=>'required',
    			        'contractedOperator'=>'required',
                	    ]);
        	    }
            catch (ValidationException $e) 
            {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' =>$e->getResponse()  
                            ],
                         ], 211);
            }
           try{
                $company =Company::create([
                				'compName'=> $request->get('compName'),
    						        'compShortName'=> $request->get('compShortName'),
    								    'rfc'=> $request->get('rfc'),
    						        'crup'=> $request->get('crup'),
    						        'firstName'=> $request->get('firstName'),
    						        'lastName'=> $request->get('lastName'),
    						        'email'=> $request->get('email'),
    						        'kind'=> $request->get('kind'),
    						        'key'=> $request->get('key'),
    						        'document'=> $request->get('document'),
    						        'street'=> $request->get('street'),
    						        'outdoorNo'=> $request->get('outdoorNo'),
    						        'interiorNo'=> $request->get('interiorNo'),
    						        'colony'=> $request->get('colony'),
    						        'city'=> $request->get('city'),
    						        'cp'=> $request->get('cp'),
    						        'country'=> $request->get('country'),
    						        'state'=> $request->get('state'),
    						        'nationality'=> $request->get('nationality'),
    						        'startDate'=> $request->get('startDate'),
    						        'paymentPeriod'=> $request->get('paymentPeriod'),
    						        'contractedDevices'=> $request->get('contractedDevices'),
    						        'contractedOperator'=> $request->get('contractedOperator'),
    			         			'userID'=>$id
                           ]);
          			  }
         		  	 catch(Exception $e)
         					   {
									       return $e;
       						     }
       		      return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'The company has been created' 
            					  ],	 ]);
				 }
      else
        		{
	        					return new JsonResponse([
	              		'apiResponse' =>[
	                	'error'=>false,
	                 	'message' => 'you can not register ' 
	            					  ],	 ]);
      			}
}
public function getCompanies()
     {
      		$data = JWTAuth::parseToken()->authenticate();
      		$userID=$data->id;
      		$obj = new Company();
      		$comp = $obj->CompaniesDetail($userID);
    	   	     return new JsonResponse([
                    'apiResponse' =>[
                    'error'=>false,                
                    'message' => 'authenticated_user'
                					],
                   'userProfile' => $data,
                   'Company Detail'=>$comp
    	 					    ]); 
     }
 public function getCompany($id)
     {
    				$data = JWTAuth::parseToken()->authenticate();
    				$userID=$data->id;
		  	  	$compid=Company::find($id);
    	 		 	if (!$compid) 
            {
    					return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Company does not found' 
                					  ],	 ]);		       		
            }
            else{
                  		$obj = new Company();
            					$comp = $obj->CompaniesDetail($userID)->where('id',$id);
      	   	     			return new JsonResponse([
                      		'apiResponse' =>[
                        		'error'=>false,                
                        		'message' => 'authenticated_user'
                    						],
                     			'userProfile' => $data,
                     			'Company Detail'=>$comp
        	 					    ]); 
          			}
  	}
public function deleteCompany($id)
    {	
      			$data = JWTAuth::parseToken()->authenticate();
      			$userID=$data->id;
         		$user = new User();
           	$role = $user->getRole($userID);
              	  foreach ($role as $role1)
             	 		{
                  	$roleName=$role1->name;
              	  }
     	if($roleName=='admin')  
      	{
				  	$comp=Company::find($id);
    	 		 	if (!$comp) 
              {
      					return new JsonResponse([
                    		'apiResponse' =>[
                      	'error'=>false,
                       	'message' => 'Company does not found' 
                  					  ],	 ]);		       		
              }
			 		$comp_id =DB::table('company')->where('id',$id)->where('userID',$userID)->delete();
   				      {

                $table1=DB::table('users')->where('id',$userID)->delete();
                }
           return new JsonResponse([
                'apiResponse' =>[
                'error'=>false,                	
                'message' => 'Company Detail id  deleted' 
                ],
            					   ]);		
   		 }
   		else
        {
        			return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'you can not Delete' 
            					  ],	 ]);
      	}
     }
public function putCompany(Request $request,$id)
   {
   	   	   $data = JWTAuth::parseToken()->authenticate();
   	   	   $userID = $data->id;
  	   	   $user = new User();
         	$role = $user->getRole($userID);
          	  foreach ($role as $role1)
         	 		{
              	$roleName=$role1->name;
         		  }
 	 if($roleName=='admin')  
       	{
				  	$comp=Company::find($id);
    	 		 	if (!$comp) {
    	 				return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Company does not found' 
                					  ],	 ]);		       		
      }
     		 	$obj = new Company();
    	    $companyID= $obj->CompaniesDetail($userID);
            foreach ($companyID as $company)
    	      	{
    	       		$compID1=$company->userID;
    	       		$compID2=$company->id;
            	
    try{
          $this->validate($request, [
	               'compName'=>'required',
  			        'compShortName'=>'required',
  	     				'rfc'=>'required',
  			        'crup'=>'required',
  			        'firstName'=>'required',
  			        'lastName'=>'required',
  			         'email'=>'required|max:255',
  			        'kind'=>'required',
  			        'key'=>'required',
  			        'document'=>'required',
  			        'street'=>'required',
  			        'outdoorNo'=>'required|max:12',
  			        'interiorNo'=>'required|max:12',
  			        'colony'=>'required',
  			        'city'=>'required',
  			        'cp'=>'required',
  			       	'country'=>'required',
  			        'state'=>'required',
  			        'nationality'=>'required',
  			        'startDate'=>'required',
  			        'paymentPeriod'=>'required',
  			        'contractedDevices'=>'required',
  			        'contractedOperator'=>'required',
                  ]);
       }
       catch (ValidationException $e) 
           {
                 return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' =>$e->getResponse()
                            ],
            ],211);
            }		
        				DB::table('company')->where('id',$id)->update([	          		
           					'compName'=> $request->get('compName'),
						        'compShortName'=> $request->get('compShortName'),
		    						'rfc'=> $request->get('rfc'),
						        'crup'=> $request->get('crup'),
						        'firstName'=> $request->get('firstName'),
						        'lastName'=> $request->get('lastName'),
						        'email'=> $request->get('email'),
						        'kind'=> $request->get('kind'),
						        'key'=> $request->get('key'),
						        'document'=> $request->get('document'),
						        'street'=> $request->get('street'),
						        'outdoorNo'=> $request->get('outdoorNo'),
						        'interiorNo'=> $request->get('interiorNo'),
						        'colony'=> $request->get('colony'),
						        'city'=> $request->get('city'),
						        'cp'=> $request->get('cp'),
						        'country'=> $request->get('country'),
						        'state'=> $request->get('state'),
						        'nationality'=> $request->get('nationality'),
						        'startDate'=> $request->get('startDate'),
						        'paymentPeriod'=> $request->get('paymentPeriod'),
						        'contractedDevices'=> $request->get('contractedDevices'),
						        'contractedOperator'=> $request->get('contractedOperator'),
						        'userID'=>$compID1,	
                				]);
              }
          	    return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'The company has been updated' 
            					  ],	 ]);
         
         }

        else
        {
        			return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'you can not update' 
            					  ],	 ]);
      	}
  }
}
