<?php

namespace App\Http\Controllers;
use App\User;
use App\Customer;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;

class CustomerController extends Controller
{
  public function postCustomer(Request $request)
    {
         	$user = JWTAuth::parseToken()->authenticate();
      		$userID=$user->id;
      	  $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='customer')  
      {
      try{
          $this->validate($request, [
        					'firstName'=>'required',
       						'lastName'=>'required',
       						'middleName'=>'required',
       							]);	
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
      
             try{
              
                 $company =Customer::create([
                    		'firstName'=> $request->get('firstName'),
           						'lastName'=> $request->get('lastName'),
           						'middleName'=> $request->get('middleName'),
           						'userId'=>$userID
          								]);	
        						}
          	 catch(Exception $e)
          	      {
          	          return $e;
                  }
               return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Customer has been Created' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not register' 
                        ],   ]);
        }
    }
    public function getCustomers()
     {
              $data = JWTAuth::parseToken()->authenticate();
              $userID=$data->id;
               $customer = new Customer();
              $CustomerDetail=$customer->CustomerDetail($userID);
                 return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false,
                         'message' => 'authenticated_user',
                         'userProfile' => $data,
                         'CustomerDetail'=>$CustomerDetail,
                             ], ]); 
    }  
  public function getCustomer($id)
     {
        $cID=Customer::find($id);
        if (!$cID) 
          {
                      return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'Customer does not found' 
                            ],   ]);             
          }
      else
          {
              $data = JWTAuth::parseToken()->authenticate();
              $userID=$data->id;
        		$customer = new Customer();
              	$CustomerDetail=$customer->CustomerDetail($userID)->where('id',$id);
                  return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false,
                         'message' => 'authenticated_user',
                         'userProfile' => $data,
                         'CustomerDetail'=>$CustomerDetail,
                             ], ]); 	
          }
  }
public function deleteCustomer($id)
     	{
           			$data = JWTAuth::parseToken()->authenticate();
           			$userID=$data->id;
                  $user = new User();
                  $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='customer')  
          {
        $cID=Customer::find($id);
        if (!$cID) 
          {
                      return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'Customer does not found' 
                            ],   ]);             
          }
      else
          {
              $data = JWTAuth::parseToken()->authenticate();
              $userID=$data->id;
                $Delete=DB::table('customer')->where('userId',$userID)->delete();
                {

                $table1=DB::table('users')->where('id',$userID)->delete();
                }
          
          return new JsonResponse([
                'apiResponse' =>[
                'error'=>false,
                 'message' => $table1
            					  ] ]);		
          
         }
     }
       else
         {
               return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'you can not Delete' 
                            ],   ]);
         }
  }
  
  public function putCustomer(Request $request,$id)
   {
            $data = JWTAuth::parseToken()->authenticate();
            $userID = $data->id;
            $user =new User();
            $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='customer')  
         {
                 $driID=Customer::find($id);
                   if (!$driID) 
                      {
                         return new JsonResponse([
                        'apiResponse' =>[
                        'error'=>false,
                        'message' => 'Customer does not found' 
                              ],   ]);             
                       }
         try{
             $this->validate($request, [
                  'firstName'=>'required',
                  'lastName'=>'required',
                  'middleName'=>'required'
                  ]); 
            }
            catch (ValidationException $e) 
            {
                 return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => $e->getResponse()
                                 ],
            ],211);
            }   
                    DB::table('customer')->where('id',$id)->where('userId',$userID)->update([            
                          'firstName'=> $request->get('firstName'),
                        'lastName'=> $request->get('lastName'),
                        'middleName'=> $request->get('middleName'),
                        'userId'=>$userID
                        ]);
       return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Customer has been Updated' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not Update' 
                        ],   ]);
        }
  }
}
