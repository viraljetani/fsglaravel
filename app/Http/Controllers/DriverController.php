<?php

namespace App\Http\Controllers;
use App\Driver;
use App\Company;
use App\Branch;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;

class DriverController extends Controller
{
  public function postDriver(Request $request)
    {
         	$user = JWTAuth::parseToken()->authenticate();
      		$userID=$user->id;
      		$obj = new Company();
          $obj1=new Branch();
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {
          if($roleName=='client')
          {

          $companyID= $obj->CompaniesDetail($userID);
              foreach ($companyID as $compID)
                 {
                   $comID1=$compID->id;
                  $brnID='0';
             
                  } 
          }

          else
          {
          $Branch= $obj1->BranchesDetail($userID);
                 foreach ($Branch as $branch)
                 {
                   $brnID=$branch->id;
                   $comID1=$branch->companyId;
              
                  } 
           }     
          try{
          $this->validate($request, [
        					'firstName'=>'required',
       						'lastName'=>'required',
       						'middleName'=>'required',
       						// 'photoOfChofer'=>'mimes:jpeg,jpg,png,gif|required|max:10000',
       						// 'licensePicture'=>'mimes:jpeg,jpg,png,gif|required|max:10000',
       						'photoOfChofer'=>'required',
       						'licensePicture'=>'required',
       						'licenseExpDate'=>'required|date_format:d/m/Y',
       						'typeLicense'=>'required',
       						'licenseNum'=>'required',
       						'rfc'=>'required',
       						'curp'=>'required',
       						'socialSecurityNo'=>'required',
       						'birthDate'=>'required|date_format:d/m/Y',
       						'placeOfBirth'=>'required',
       						'street'=>'required',
       						'outdoorNo'=>'required',
       						'interiorNo'=>'required',
       						'colony'=>'required',
       						'city'=>'required',
       						'cp'=>'required',
       						'country'=>'required',
       						'state'=>'required',
       						'nationality'=>'required',
                  'userID'=>'required'
       						]);	
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
      
             try{
              
                 $company =Driver::create([
                    	'firstName'=> $request->get('firstName'),
           						'lastName'=> $request->get('lastName'),
           						'middleName'=> $request->get('middleName'),
           						'photoOfChofer'=> $request->get('photoOfChofer'),
           						'licensePicture'=> $request->get('licensePicture'),
           						'licenseExpDate'=> $request->get('licenseExpDate'),
           						'typeLicense'=> $request->get('typeLicense'),
           						'licenseNum'=> $request->get('licenseNum'),
           						'rfc'=> $request->get('rfc'),
           						'curp'=> $request->get('curp'),
           						'socialSecurityNo'=> $request->get('socialSecurityNo'),
           						'birthDate'=> $request->get('birthDate'),
           						'placeOfBirth'=> $request->get('placeOfBirth'),
           						'street'=> $request->get('street'),
           						'outdoorNo'=> $request->get('outdoorNo'),
           						'interiorNo'=> $request->get('interiorNo'),
           						'colony'=> $request->get('colony'),
           						'city'=> $request->get('city'),
           						'cp'=> $request->get('cp'),
           						'country'=> $request->get('country'),
           						'state'=> $request->get('state'),
           						'nationality'=> $request->get('nationality'),
                      			'action'=>'0',
           						'userID'=>$request->get('userID'),
          						  'companyID'=>$comID1,
          	          			'branchID'=>$brnID
          	 							]);	
        						}

          	 catch(Exception $e)
          	      {
          	          return $e;
                  }
              
               return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Driver has been Created' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not register' 
                        ],   ]);
        }
    }
   public function getDrivers()
     {
              $data = JWTAuth::parseToken()->authenticate();
              $userID=$data->id;
              $obj = new Driver();
              $obj1 = new Company();
              $obj2 = new Branch();
              $drivers=$obj->DriverDetail($userID);
              $comp = $obj1->CompaniesDetail($userID);
              $Branch= $obj2->BranchesDetail($userID);  
                 return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false
                                  ],
                         'message' => 'authenticated_user',
                         'userProfile' => $data,
                         'companyDetail'=>$comp,
                         'BranchesDetail'=>$Branch,
                         'driverDetail'=>$drivers,
                             ]); 
    }  
  public function getDriver($id)
     {
        $driID=Driver::find($id);
        if (!$driID) 
          {
                      return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'Driver does not found' 
                            ],   ]);             
          }
      else
          {
              $data = JWTAuth::parseToken()->authenticate();
              $userID=$data->id;
              $obj = new Driver();
              $obj1 = new Company();
              $obj2 = new Branch();
            $comp = $obj1->CompaniesDetail($userID);
            $drivers=$obj->DriverDetail($userID)->where('id',$id);
             $Branch= $obj2->BranchesDetail($userID);
               return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false
                              ],
                    'message' => 'authenticated_user',
                   'userProfile' => $data,
                   'companyDetail'=>$comp,
                   'BranchesDetail'=>$Branch,
                   'driverDetail'=>$drivers,
                      ]); 
          }
  }
  public function deleteDriver($id)
     	{
            			$data = JWTAuth::parseToken()->authenticate();
            			$userID=$data->id;
                  $user = new User();
                  $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='client' OR $roleName=='branch')  
          {
                   $driID=Driver::find($id);
                     if (!$driID) 
                      {
                        return new JsonResponse([
                        'apiResponse' =>[
                        'error'=>false,
                        'message' => 'Driver does not found' 
                              ],   ]);             
                         }
                  $Delete=DB::table('drivers')->where('id',$id)->where('userId',$userID)->delete();
                {

                $table1=DB::table('users')->where('id',$userID)->delete();
                }
          return new JsonResponse([
      
               'apiResponse' =>[
                'error'=>false,
                 'message' => 'Driver  id  deleted' 
            					  ] ]);		
         }
       else
         {
               return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'you can not Delete' 
                            ],   ]);
         }
  }
  
  public function putDriver(Request $request,$id)
   {
            $data = JWTAuth::parseToken()->authenticate();
            $userID = $data->id;
            $obj = new Company();
            $obj1=new Branch();
            $obj2 = new Driver();
            $user = new User();
            $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='client' OR $roleName=='branch')  
         {
                 $driID=Driver::find($id);
                   if (!$driID) 
                      {
                         return new JsonResponse([
                        'apiResponse' =>[
                        'error'=>false,
                        'message' => 'Driver does not found' 
                              ],   ]);             
                       }
          $companyID= $obj->CompaniesDetail($userID);
          $Branch= $obj1->BranchesDetail($userID);
           $driver = $obj2->DriverDetail($userID);
              foreach ($driver as $driver1)
                   {
                      $driverID=$driver1->id;
                    }
              foreach ($companyID as $company)
                 {
                    $compID1=$company->id;
                  }  
             foreach ($Branch as $branch)
                 {
                   $brnID=$branch->id;
                  } 
         try{
             $this->validate($request, [
                  'firstName'=>'required',
                  'lastName'=>'required',
                  'middleName'=>'required',
                  // 'photoOfChofer'=>'mimes:jpeg,jpg,png,gif|required|max:10000',
                  // 'licensePicture'=>'mimes:jpeg,jpg,png,gif|required|max:10000',
                  'photoOfChofer'=>'required',
                  'licensePicture'=>'required',
                  'licenseExpDate'=>'required|date_format:d/m/Y',
                  'typeLicense'=>'required',
                  'licenseNum'=>'required',
                  'rfc'=>'required',
                  'curp'=>'required',
                  'socialSecurityNo'=>'required',
                  'birthDate'=>'required|date_format:d/m/Y',
                  'placeOfBirth'=>'required',
                  'street'=>'required',
                  'outdoorNo'=>'required',
                  'interiorNo'=>'required',
                  'colony'=>'required',
                  'city'=>'required',
                  'cp'=>'required',
                  'country'=>'required',
                  'state'=>'required',
                  'nationality'=>'required',
                  ]); 
            }
            catch (ValidationException $e) 
            {
                 return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' => $e->getResponse()
                                 ],
            ],211);
            }   
                    DB::table('drivers')->where('id',$id)->where('userID',$userID)->update([            
                          'firstName'=> $request->get('firstName'),
                        'lastName'=> $request->get('lastName'),
                        'middleName'=> $request->get('middleName'),
                        'photoOfChofer'=> $request->get('photoOfChofer'),
                        'licensePicture'=> $request->get('licensePicture'),
                        'licenseExpDate'=> $request->get('licenseExpDate'),
                        'typeLicense'=> $request->get('typeLicense'),
                        'licenseNum'=> $request->get('licenseNum'),
                        'rfc'=> $request->get('rfc'),
                        'curp'=> $request->get('curp'),
                        'socialSecurityNo'=> $request->get('socialSecurityNo'),
                        'birthDate'=> $request->get('birthDate'),
                        'placeOfBirth'=> $request->get('placeOfBirth'),
                        'street'=> $request->get('street'),
                        'outdoorNo'=> $request->get('outdoorNo'),
                        'interiorNo'=> $request->get('interiorNo'),
                        'colony'=> $request->get('colony'),
                        'city'=> $request->get('city'),
                        'cp'=> $request->get('cp'),
                        'country'=> $request->get('country'),
                        'state'=> $request->get('state'),
                        'nationality'=> $request->get('nationality'),
                        'action'=>'0',
                        // '	userID'=>$userID,
                        // 'companyID'=>$compID1,
                        // 'branchID'=>$brnID
                        ]);
       return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Driver has been Updated' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not Update' 
                        ],   ]);
        }
  }
}
