<?php

namespace App\Http\Controllers;
use App\User;
use App\Expanse;		
use App\Driver;		
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;

class ExpanseController extends Controller
{
     public function postExpanse(Request $request)
   {
         		  $user = JWTAuth::parseToken()->authenticate();
      		      $id=$user->id;
      	     	  $obj = new User();
                  $role = $obj->getRole($id);
                   foreach ($role as $role1)
                   {
                      $roleName=$role1->name;
                    }
        if($roleName=='admin')    
        {
              	try{
 		  	        $this->validate($request, ['typeOfService'=>'required',]);
 		  	    }												
 		  	     catch (ValidationException $e) 
    	        {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' =>$e->getResponse()  
                            ],
                         ], 211);
        	    }
           try{
       		        $expanse =Expanse::create(['typeOfService'=> $request->get('typeOfService')]);
    			     }
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		      return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'The Expanse has been created' 
            					  ],	 ]);
				 }
      else
        		{
     					return new JsonResponse([
	              		'apiResponse' =>[
	                	'error'=>false,
	                 	'message' => 'Expanse can not inserted ' 
	            					  ],	 ]);
            	}
		}
	public function getExpanses()
     {
      		$data = JWTAuth::parseToken()->authenticate();
      		$userID=$data->id;
      		$obj = new expanse();
      		$expanse = $obj->expanseDetail();
    	   	     return new JsonResponse([
                    'apiResponse' =>[
                    'error'=>false,                
                    'message' => 'authenticated_user'
                					],
                   'userProfile' => $data,
                   'Company Detail'=>$expanse
    	 					    ]); 
}
 public function getExpanse($id)
     {
    				$data = JWTAuth::parseToken()->authenticate();
    				$userID=$data->id;
		  	  	$expanse=Expanse::find($id);
    	 		 	if (!$expanse) 
            		{
    					return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Expanse does not found' 
                					  ],	 ]);		       		
            		}
            else{
                  		$obj = new Expanse();
            					$exp = $obj->expanseDetail()->where('id',$id);
      	   	     			return new JsonResponse([
                      		'apiResponse' =>[
                        		'error'=>false,                
                        		'message' => 'authenticated_user'
                    						],
                     			'userProfile' => $data,
                     			'expanse Detail'=>$exp
        	 					    ]); 
          			}
  	}
public function deleteExpanse($id)
    {	
      			$data = JWTAuth::parseToken()->authenticate();
      			$userID=$data->id;
         		$user = new User();
	           	$role = $user->getRole($userID);
              	  foreach ($role as $role1)
             	 		{
                  	$roleName=$role1->name;
              	  }
              	  
     	if($roleName=='admin')  
      	{
				  	$Expanse=Expanse::find($id);
    	 		 	if (!$Expanse) 
              			{
      					return new JsonResponse([
                    		'apiResponse' =>[
                      		'error'=>false,
                       		'message' => 'Expanse does not found' 
                  					  ],	 ]);		       		
              			}
			 		$comp_id =DB::table('expanse')->where('id',$id)->delete();
   				  return new JsonResponse([
                'apiResponse' =>[
                'error'=>false,                	
                'message' => 'Expanse Detail id  deleted' 
                ],
            	]);		
   		 }
   		else
        {
        			return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'you can not Delete' 
            			  ],	 ]);
           }
     }
    public function putExpanse(Request $request,$id)
   {

   	   	   $data = JWTAuth::parseToken()->authenticate();
   	   	   $userID = $data->id;
  	   	   $user = new User();
         	$role = $user->getRole($userID);
          	  foreach ($role as $role1)
         	 		{
              	$roleName=$role1->name;
         		  }
 	 if($roleName=='admin')  
       	{
				  	$Expanse=Expanse::find($id);
    	 		 	if (!$Expanse) {
    	 				return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Expanse does not found' 
                					  ],	 ]);		       		
      }
    		try{
    		     $this->validate($request, ['typeOfService'=>'required',]);
 		 	    }												
 		  	     catch (ValidationException $e) 
    	        {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' =>$e->getResponse()  
                            ],
                         ], 211);
        	    }
           try{
       		        DB::table('expanse')->where('id',$id)->update([	          		
              				'typeOfService'=> $request->get('typeOfService')
              								]);
      			}
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		      return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'The Expanse has been Updated' 
            					  ],	 ]);
				 }
      else
        		{
     					return new JsonResponse([
	              		'apiResponse' =>[
	                	'error'=>false,
	                 	'message' => 'Expanse can not Updated ' 
	            					  ],	 ]);
            	}
		}

	//  Asign  vaucher

	public function getVaucherInfo($id)
	{
		$data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
               $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
     	if($roleName=='admin')  
       		// if($roleName=='client' OR $roleName=='branch')  
      {
      				$drivers=Driver::find($id);
    	 		 	if (!$drivers) 
            		{
    					return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Drivers does not found' 
                					  ],	 ]);		       		
            		}
       else{
       	try{
            $driver = DB::table('drivers')->join('asignvaucher','asignvaucher.driverId','=','drivers.id')->where('drivers.id',$id)->where('userID',$userID)->select('drivers.*')->first();
	           $driver1 = DB::table('asignvaucher')
           					->join('drivers','asignvaucher.driverId','=','drivers.id')
           					->join('expanse','asignvaucher.expanseId','=','expanse.id')
           					->join('vaucher','asignvaucher.vaucherId','=','vaucher.id')
           					->select('expanse.typeOfService', 'vaucher.vaucherName')
           					->where('drivers.id',$id)->where('userID',$userID)->get();
         	}
     	   catch(Exception $e)	
        	{
            echo $e;
        	}
      		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                  		'Driver'=>$driver,
                  		'asignvaucher'=>$driver1
                  			],
                      ],   ]);
   				 }
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => ' not asign' 
                        ],   ]);
        }
	}
public function asignVaucher(Request $request)
	  {
    			 $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
              	 $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }

        // if($roleName=='client' OR $roleName=='branch')  
   	if($roleName=='admin')  
      {  
      		try{	
   				  $this->validate($request, [
                             				 'driverId'=>'required', 
    	           							 'vaucherId'=>'required', 
    	           							 'expanseId'=>'required', 
    	           										]);
   				 }
   				  	       catch (ValidationException $e) 
					            {
					            return new JsonResponse([
				  	                'apiResponse' =>[
					                  'error'=>true,
					                  'message'=>$e->getResponse()    
                        					 ], ],211);
					            }
    	         try{
               		$vid=$request->get('vehiclesId');
               		$did=$request->get('driverId');
            
                   $vehiclesAsign = DB::table('asignvaucher')->join('drivers','drivers.id','=','asignvaucher.driverId')->where('drivers.userID',$userID)->insert(['driverId'=>$request->get('driverId'),
                  													 'vaucherId'=>$request->get('vaucherId'),
                  													'expanseId'=>$request->get('expanseId'),
                  														]);
                 }
          catch(Exception $e)
                     {
                              return $e;
                      }
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>'asign Vaucher '
                        ],   ]);
         	}
            else
	        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => ' not asign' 
                        ],   ]);
        }
	}

public function putAsignVaucher(Request $request,$id)
	  {
    			 $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
              	 $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
    // if($roleName=='client' OR $roleName=='branch')  
   	if($roleName=='admin')  
      {  
				  	$asignvaucher=DB::table('asignvaucher')->where('driverId',$id)->get();
  		  	foreach ($asignvaucher as $asign1)
				  	 {
				  		$asign1=$asign1->id;	
				  	}
    	 		 		if (!$asign1) {
    	 				return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Vaucher does not found' 
                					  ],	 ]);		       		
     				 }
     			else
     			{
     				try{	
   						  $this->validate($request, [
                             				 'driverId'=>'required', 
    	           							 'vaucherId'=>'required', 
    	           							 'expanseId'=>'required', 
    	           										]);
   							 }
   				  	       catch (ValidationException $e) 
					            {
					            return new JsonResponse([
				  	                'apiResponse' =>[
					                  'error'=>true,
					                  'message'=>$e->getResponse()    
                        					 ], ],211);
					            }
               try{
               		   $vaucherAsign = DB::table('asignvaucher')
                              ->join('drivers','drivers.id','=','asignvaucher.driverId')
                              			->where('drivers.id',$id)
                              			->update(['driverId'=>$request->get('driverId'),
                  											'vaucherId'=>$request->get('vaucherId'),
                  											'expanseId'=>$request->get('expanseId'),
                  														]);
                 }
          catch(Exception $e)
                     {
                              return $e;
                      }
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>'asign Vaucher Updated '
                        ],   ]);
      			}
         }
            else
	        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => ' not asign' 
                        ],   ]);
        }
	}
 public function deleteAsignVaucher($id)
	{
		$data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
               $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
	if($roleName=='admin')  
       		// if($roleName=='client' OR $roleName=='branch')  
      {
      				$drivers=Driver::find($id);
    	 		 	if (!$drivers) 
            		{
    					return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Drivers does not found' 
                					  ],	 ]);		       		
            		}
       else{
       		try{
             $vaucherAsign = DB::table('asignvaucher')
                              ->join('drivers','drivers.id','=','asignvaucher.driverId')
                             ->where('drivers.userID',$userID)->where('drivers.id',$id)
                             ->delete();
	       }
     	   catch(Exception $e)
        	{
            echo $e;
        	}
        return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>'asign Vaucher has been deleted'
                      ],   ]);
   				 }
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => ' not asign' 
                        ],   ]);
        }
	}
}
