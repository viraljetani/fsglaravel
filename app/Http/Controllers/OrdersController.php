<?php

namespace App\Http\Controllers;
use App\TripsHistory;
use App\User;
use App\Orders;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;
class OrdersController extends Controller
{
 public function postOrders(Request $request)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   
        try{
          $this->validate($request, [
                   'name'=>'required',
                  'type'=>'required',
                  'dropLocation'=>'required',
                   'customerID'=>'required',
                   'expectedDeliveryDate'=>'required|date_format:d/m/Y'
                   ]); 
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
      	try{
       		        $expanse =Orders::create([
       		        								'name'=> $request->get('name'),
       		    									'type'=>$request->get('type'),
								                 'dropLocation'=>$request->get('dropLocation'),
								                   'customerID'=>$request->get('customerID'),
								                   'expectedDeliveryDate'=>$request->get('expectedDeliveryDate')
								               ]);
    			     }
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		
               return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Orders  has been Created' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
     public function getOrders()
    {
          $user1 = JWTAuth::parseToken()->authenticate();
          $userID=$user1->id;
          $user = new User();
  		  $order= new Orders();
  		  $ordersDetail=$order->OrdersDetail($userID);
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   
         
              return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false
                                  ],
                         'message' => 'authenticated_user',
                         'userProfile' => $user1,
                         'orderDetail'=>$ordersDetail,
                             ]); 
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
     public function getOrder($id)
    {
          $user1 = JWTAuth::parseToken()->authenticate();
          $userID=$user1->id;
          $user = new User();
  		  $order= new Orders();
  		  $ordersDetail=$order->OrdersDetail($userID)->where('id',$id);
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {    $Orders=Orders::find($id);
        if (!$Orders) 
          {
                      return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'Order does not found' 
                            ],   ]);             
          }
              return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false
                                  ],
                         'message' => 'authenticated_user',
                         'userProfile' => $user1,
                         'orderDetail'=>$ordersDetail,
                             ]); 
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
 public function putOrders(Request $request,$id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   
      	  $Orders=Orders::find($id);
        if (!$Orders) 
          {
                      return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'Order does not found' 
                            ],   ]);             
          }
        try{
          $this->validate($request, [
                   'name'=>'required',
                  'type'=>'required',
                  'dropLocation'=>'required',
                   'customerID'=>'required',
                   'expectedDeliveryDate'=>'required|date_format:d/m/Y'
                   ]); 
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
      	try{
       		        $Orders =Orders::where('id',$id)->update([
       		        								'name'=> $request->get('name'),
       		    									'type'=>$request->get('type'),
								                 'dropLocation'=>$request->get('dropLocation'),
								                   'customerID'=>$request->get('customerID'),
								                   'expectedDeliveryDate'=>$request->get('expectedDeliveryDate')
								               ]);
    			     }
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		
               return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Orders  has been updated' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
  public function deleteOrders($id)
    {
          $user1 = JWTAuth::parseToken()->authenticate();
          $userID=$user1->id;
          $user = new User();
  		  $order= new Orders();
  		  $ordersDetail=$order->OrdersDetail()->where('id',$id);
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {    $Orders=Orders::find($id);
        if (!$Orders) 
          {
                      return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'Order does not found' 
                            ],   ]);             
          }
          	  	$deleteOrders=DB::table('orders')->where('id',$id)->delete();
              return new JsonResponse([
                          'apiResponse' =>[
                          'error'=>false,
                         	'message' => 'Orders has been deleted'
                                  ],
                             ]); 
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
    //Drop Orders
    
}
