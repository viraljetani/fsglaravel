<?php
namespace App\Http\Controllers;
use App\Company;
use App\Branch;
use App\User;
use App\Driver;
use App\Vehicles;
use App\Trip;
use App\Orders;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;
class ServicesController extends Controller
{
	public function getservices()
  	{
     $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
               $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='admin')  
      {
      		// $User=User::where('id',$userID)->count();
           $User=User::count();
          $Company=Company::count();
      		$Branch=Branch::count();
      		$Driver=Driver::count();

      		$Vehicles=Vehicles::count();
          $Orders=Orders::count();
      		$Trip=Trip::count();
          $tr = DB::table('trips')
                    ->join('drivers','trips.assignedDriver','=','drivers.id')
                    ->join('vehicles','trips.assignedVehicle','=','vehicles.id')->count();
          // $tr = DB::table('trips')
          //           ->join('drivers','trips.assignedDriver','=','drivers.id')
          //           ->join('vehicles','trips.assignedVehicle','=','vehicles.id')
          //           ->where('drivers.userID',$userID)->where('vehicles.userID',$userID)->count();
           	    
      		return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
               	 'Users'=>$User,
                  	'company'=> $Company,
                  	'Branch'=>$Branch,
                  	'Driver'=>$Driver,
                  	'Vehicles'=>$Vehicles,
                    'Orders'=>$Orders,
                  	'Trip'=>$tr
					],  ], ]);
     }
 else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }

}