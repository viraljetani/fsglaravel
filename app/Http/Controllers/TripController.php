<?php

namespace App\Http\Controllers;
use App\Company;
use App\Branch;
use App\User;
use App\Driver;
use App\Vehicles;
use App\Trip;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;
class TripController extends Controller
{
	public function getVehiclesDriver()
  	{
     $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
               $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='client' OR $roleName=='branch')  
      {
          if($roleName=='client')
          {
 
                      //action o for deActivate And 1 for activate
        $vehicles=DB::table("vehicles")->join('company','company.id','=','vehicles.companyId')->where('vehicles.action','0' )->where('company.userID',$userID)->select('vehicles.id')->get();
        // $driver=DB::table("drivers")->where('action','=','0')->where('companyId','=',$userID)->select('id')->get();
        // $vehicles=Vehicles::select('id')->where('action','=','0')->where('companyId',$userID)->get();
       $driver=DB::table("drivers")->join('company','company.id','=','drivers.companyId')->where('drivers.action','0' )->where('company.userID',$userID)->select('drivers.id')->get();
       
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                    'vehicles'=>$vehicles,
                  'driver'=>$driver
                          ],
                        ],   ]);
          }

          else
          {
               //action o for deActivate And 1 for activate
        $vehicles=DB::table("vehicles")->join('branch','branch.id','=','vehicles.branchId')->where('vehicles.action','0' )->where('branch.userID',$userID)->select('vehicles.id')->get();
        
        $driver=DB::table("drivers")->join('branch','branch.id','=','drivers.branchId')->where('drivers.action','0' )->where('branch.userID',$userID)->select('drivers.id')->get();
       
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                    'vehicles'=>$vehicles,
                  'driver'=>$driver
                          ],
                        ],   ]);
                  }  
       }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => ' not asign' 
                        ],   ]);
        }
  }
    public function postTrips(Request $request)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $obj = new Company();
          $obj1=new Branch();
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   


                   try{
          $this->validate($request, [
                   'name'=>'required',
                  'startLocation'=>'required',
                  'startDate'=>'required|date_format:d/m/Y',
                   'note'=>'required',
                  'assignedDriver'=>'required',
                  'assignedVehicle'=>'required',
                   ]); 
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
               
              if($roleName=='client')
          {

          $companyID= $obj->CompaniesDetail($userID);
              foreach ($companyID as $compID)
                 {
                   $compID1=$compID->id;
                  $brnID='0';
             
                  } 
      
             try{
                  $did=$request->get('assignedDriver');
                $vid=$request->get('assignedVehicle');
                 $trips = DB::table('trips')->insert(['name'=> $request->get('name'),
                                        'startLocation'=> $request->get('startLocation'),
                                      'startDate'=> $request->get('startDate'),
                                      'status'=> 'assigned',
                                      'note'=> $request->get('note'),
                                      'assignedDriver'=> $request->get('assignedDriver'),
                                      'assignedVehicle'=>$request->get('assignedVehicle'),
                                      'companyId'=>$compID1,
                                      'branchId'=>$brnID,
                                  ]);
                 {

                    $driversUpdate=DB::table('drivers')->join('company','company.id','=','drivers.companyId')->where('drivers.id',$did)->where('company.userID',$userID)->update(['action'=>'1']);
                   $vehicleUpdate=DB::table('vehicles')->join('company','company.id','=','vehicles.companyId')->where('vehicles.id',$vid)->where('company.userID',$userID)->update(['action'=>'1']);
                  }
                    }
             catch(Exception $e)
                  {
                      return $e;
                  }
           }

          else
          {
          $Branch= $obj1->BranchesDetail($userID);
                 foreach ($Branch as $branch)
                 {
                   $brnID=$branch->id;
                   $compID1=$branch->companyId;
                    }
          
             try{
                  $did=$request->get('assignedDriver');
                $vid=$request->get('assignedVehicle');
                 $trips = DB::table('trips')->insert(['name'=> $request->get('name'),
                                        'startLocation'=> $request->get('startLocation'),
                                      'startDate'=> $request->get('startDate'),
                                      'status'=> 'assigned',
                                      'note'=> $request->get('note'),
                                      'assignedDriver'=> $request->get('assignedDriver'),
                                      'assignedVehicle'=>$request->get('assignedVehicle'),
                                      'companyId'=>$compID1,
                                      'branchId'=>$brnID,
                                  ]);
                 {
                    $driversUpdate=DB::table('drivers')->join('branch','branch.id','=','drivers.branchID')->where('drivers.id',$vid)->where('branch.userID',$userID)->update(['action'=>'1']);
                  $vehicleUpdate=DB::table('vehicles')->join('branch','branch.id','=','vehicles.branchID')->where('vehicles.id',$vid)->where('branch.userID',$userID)->update(['action'=>'1']);
                  
                  }
                    }
             catch(Exception $e)
                  {
                      return $e;
                  }
           }     
            return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Trips has been Created' 
                        ],   ]);
              
                  } 
          
        
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not register' 
                        ],   ]);
        }
    }
      public function getTrips()
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   
             if($roleName=='client')
          {

              try{ 
          
                 $tr = DB::table('drivers')
                    ->join('trips','trips.assignedDriver','=','drivers.id')
                      ->join('company','company.id','=','drivers.companyID')->where('company.userID',$userID)->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
             $driver1 = DB::table('drivers')
                    ->join('trips','trips.assignedDriver','=','drivers.id')
                    ->join('company','company.id','=','drivers.companyID')->where('company.userID',$userID)
                    ->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
                    ->get();
                   $vehicles1=DB::table('vehicles')
                    ->join('trips','trips.assignedVehicle','=','vehicles.id')
                    ->join('company','company.id','=','vehicles.companyId')->where('company.userID',$userID)
                    ->select('vehicles.id','vehicles.firstName')
                    ->get();
          }
         catch(Exception $e)  
          {
            echo $e;
          }
            }
            else
            {
              try{ 
         	
                 $tr = DB::table('drivers')
                 		->join('trips','trips.assignedDriver','=','drivers.id')
                      ->join('branch','branch.id','=','drivers.branchId')->where('branch.userID',$userID)->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
 	           $driver1 = DB::table('drivers')
           					->join('trips','trips.assignedDriver','=','drivers.id')
                    ->join('branch','branch.id','=','drivers.branchId')->where('branch.userID',$userID)
           					->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
           			    ->get();
           		     $vehicles1=DB::table('vehicles')
           					->join('trips','trips.assignedVehicle','=','vehicles.id')
                    ->join('branch','branch.id','=','vehicles.branchId')->where('branch.userID',$userID)
                    ->select('vehicles.id','vehicles.firstName')
           					->get();
         	}
     	   catch(Exception $e)	
        	{
            echo $e;
        	}
        }
      		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                  		'trips'=>$tr,
                  		'driver'=>$driver1,
                  		'vehicles'=>$vehicles1
                  			],
                      ],   ]);
                 	 }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
      public function getTrip($id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   
            $asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}	
        
             if($roleName=='client')
          {

              try{ 
          
                 $tr = DB::table('drivers')
                    ->join('trips','trips.assignedDriver','=','drivers.id')
                      ->join('company','company.id','=','drivers.companyID')->where('company.userID',$userID)->where('trips.id',$id)->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
             $driver1 = DB::table('drivers')
                    ->join('trips','trips.assignedDriver','=','drivers.id')
                    ->join('company','company.id','=','drivers.companyID')->where('company.userID',$userID)->where('trips.id',$id)
                    ->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
                    ->get();
                   $vehicles1=DB::table('vehicles')
                    ->join('trips','trips.assignedVehicle','=','vehicles.id')
                    ->join('company','company.id','=','vehicles.companyId')->where('company.userID',$userID)->where('trips.id',$id)
                    ->select('vehicles.id','vehicles.firstName')
                    ->get();
          }
         catch(Exception $e)  
          {
            echo $e;
          }
            }
            else
            {
              try{ 
          
                 $tr = DB::table('drivers')
                    ->join('trips','trips.assignedDriver','=','drivers.id')
                      ->join('branch','branch.id','=','drivers.branchId')->where('branch.userID',$userID)->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
             $driver1 = DB::table('drivers')
                    ->join('trips','trips.assignedDriver','=','drivers.id')
                    ->join('branch','branch.id','=','drivers.branchId')->where('branch.userID',$userID)
                    ->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
                    ->get();
                   $vehicles1=DB::table('vehicles')
                    ->join('trips','trips.assignedVehicle','=','vehicles.id')
                    ->join('branch','branch.id','=','vehicles.branchId')->where('branch.userID',$userID)
                    ->select('vehicles.id','vehicles.firstName')
                    ->get();
          }
         catch(Exception $e)  
          {
            echo $e;
          }
        }	  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                  		'trips'=>$tr,
                  		'driver'=>$driver1,
                  		'vehicles'=>$vehicles1
                  		
                  			],
                      ],   ]);
   				 }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing ' 
                        ],   ]);
        }
    }
  public function putTrips(Request $request,$id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $obj = new Company();
          $obj1=new Branch();
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   
      	$asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}
                           
      try{
          $this->validate($request, [
                   'name'=>'required',
                  'startLocation'=>'required',
                  'startDate'=>'required|date_format:d/m/Y',
                  'status'=>'required',
                  'note'=>'required',
                  'assignedDriver'=>'required',
                  'assignedVehicle'=>'required',
                  ]); 
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
          
                      if($roleName=='client')
          {

          $companyID= $obj->CompaniesDetail($userID);
              foreach ($companyID as $compID)
                 {
                   $compID1=$compID->id;
                  $brnID='0';
             
                  } 
      
             try{
                  $did=$request->get('assignedDriver');
                 $vid=$request->get('assignedVehicle');
                $trips = DB::table('trips')->where('id',$id)->update(['name'=> $request->get('name'), 
                                        'startLocation'=> $request->get('startLocation'),
                                      'startDate'=> $request->get('startDate'),
                                      'status'=> $request->get('status'),
                                      'note'=> $request->get('note'),
                                      'assignedDriver'=> $request->get('assignedDriver'),
                                      'assignedVehicle'=>$request->get('assignedVehicle'),
                                      'companyId'=>$compID1,
                                      'branchId'=>$brnID
                                  ]);
                  {
                    $driversUpdate=DB::table('drivers')->join('company','company.id','=','drivers.companyId')->where('drivers.id',$did)->where('company.userID',$userID)->update(['action'=>'1']);
                   $vehicleUpdate=DB::table('vehicles')->join('company','company.id','=','vehicles.companyId')->where('vehicles.id',$vid)->where('company.userID',$userID)->update(['action'=>'1']);
                  }
            }
             catch(Exception $e)
                  {
                      return $e;
                  }
           }

          else
          {
          $Branch= $obj1->BranchesDetail($userID);
                 foreach ($Branch as $branch)
                 {
                   $brnID=$branch->id;
                   $compID1=$branch->companyId;
                    }
          
             try{
                     $did=$request->get('assignedDriver');
                 $vid=$request->get('assignedVehicle');
              $trips = DB::table('trips')->where('id',$id)->update(['name'=> $request->get('name'), 
                                        'startLocation'=> $request->get('startLocation'),
                                      'startDate'=> $request->get('startDate'),
                                      'status'=> $request->get('status'),
                                      'note'=> $request->get('note'),
                                      'assignedDriver'=> $request->get('assignedDriver'),
                                      'assignedVehicle'=>$request->get('assignedVehicle'),
                                      'companyId'=>$compID1,
                                      'branchId'=>$brnID,
                                  ]);
                 {
                       
                    $driversUpdate=DB::table('drivers')->join('branch','branch.id','=','drivers.branchID')->where('drivers.id',$vid)->where('branch.userID',$userID)->update(['action'=>'1']);
                  $vehicleUpdate=DB::table('vehicles')->join('branch','branch.id','=','vehicles.branchID')->where('vehicles.id',$vid)->where('branch.userID',$userID)->update(['action'=>'1']);
                  
                  }
                    }
             catch(Exception $e)
                  {
                      return $e;
                  }
           }     
               return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Trips has been updated' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not updated' 
                        ],   ]);
        }
    }
   public function deleteTrips(Request $request,$id)
    {
            $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
                 $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
         if($roleName=='client' OR $roleName=='branch')  
      {   
         $asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}	
            foreach ($asign11 as $asign) 
            {
            $vid=$asign->assignedVehicle;
         	$did=$asign->assignedDriver;
         	try{
             if($roleName='client')
             {
  
                     $trips = DB::table('trips')->where('id',$id)->delete();
                  $driversUpdate=DB::table('drivers')->join('company','company.id','=','drivers.companyId')->where('drivers.id',$did)->where('company.userID',$userID)->update(['action'=>'0']);
                   $vehicleUpdate=DB::table('vehicles')->join('company','company.id','=','vehicles.companyId')->where('vehicles.id',$vid)->where('company.userID',$userID)->update(['action'=>'0']);
                              }
              else
              {          $trips = DB::table('trips')->where('id',$id)->delete();
            
                    $driversUpdate=DB::table('drivers')->join('branch','branch.id','=','drivers.branchID')->where('drivers.id',$vid)->where('branch.userID',$userID)->update(['action'=>'0']);
                  $vehicleUpdate=DB::table('vehicles')->join('branch','branch.id','=','vehicles.branchID')->where('vehicles.id',$vid)->where('branch.userID',$userID)->update(['action'=>'0']);
            
              }
            }
          catch(Exception $e)
                     {
                              return $e;
                      }
                  }
      return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'our trips has been deleted'
                        ],   ]);
          }
            else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => ' not asign' 
                        ],   ]);
        }
  }
}
