<?php

namespace App\Http\Controllers;
use App\TripsHistory;
use App\User;
use App\Trip;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;
class TripsHistoryController extends Controller
{
 public function postTriphistory(Request $request ,$id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   	
      	$asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}
        try{
          $this->validate($request, [
                  'type'=>'required',
                  'location'=>'required',
                  'image'=>'required',
                  'reference'=>'required',
                  
                   ]); 
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
      
              try{
       		        $expanse =TripsHistory::create([
       		     									'type'=>$request->get('type'),
								                 	'location'=>$request->get('location'),
								                   'image'=>$request->get('image'),
								            		'reference'=>$request->get('reference'),
								                   'tripId'=>$id,
								               ]);
    			     }
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Trips History  has been Created' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
        public function getTriphistories()
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {  
           try{ 
          
      	if($roleName=='client')
      	{
           	        $tr = DB::table('trips')
                 		->join('company','company.id','=','trips.companyId')->where('company.userID',$userID)
                 		->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
           	        $TripsHistory = DB::table('trips')
                 		->join('triphistory','trips.id','=','triphistory.tripId')
                 		->join('company','company.id','=','trips.companyId')->where('company.userID',$userID)
                 		->select('triphistory.*')->get();
 	           $driver1 = DB::table('trips')
           					->join('drivers','trips.assignedDriver','=','drivers.id')
           					->join('company','company.id','=','drivers.companyID')->where('company.userID',$userID)
                 			->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
           					->get();
           		$vehicles1=DB::table('trips')
           					->join('vehicles','trips.assignedVehicle','=','vehicles.id')
           					->join('company','company.id','=','vehicles.companyID')->where('company.userID',$userID)
                 			->select('vehicles.id','vehicles.firstName')
           					->get();
         }
         else
         {
                  $tr = DB::table('trips')
                 		->join('branch','branch.id','=','trips.branchId')->where('branch.userID',$userID)
                 		->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
           	        $TripsHistory = DB::table('trips')
                 		->join('triphistory','trips.id','=','triphistory.tripId')
                 		->join('branch','branch.id','=','trips.branchId')->where('branch.userID',$userID)
                 		->select('triphistory.*')->get();
 	           $driver1 = DB::table('trips')
           					->join('drivers','trips.assignedDriver','=','drivers.id')
           					->join('branch','branch.id','=','drivers.branchID')->where('branch.userId',$userID)
                 			->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
           					->get();
           		$vehicles1=DB::table('trips')
           					->join('vehicles','trips.assignedVehicle','=','vehicles.id')
           					->join('branch','branch.id','=','vehicles.branchID')->where('branch.userId',$userID)
                 			->select('vehicles.id','vehicles.firstName')
           					->get();
         }	
     }
     	   catch(Exception $e)	
        	{
            echo $e;
        	}
      		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                  		'trips'=>$tr,
                  		'TripsHistory'=>$TripsHistory,
                  		'driver'=>$driver1,
                  		'vehicles'=>$vehicles1
                  			],
                      ],   ]);
                 	 }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    } 
      public function getTriphistory($id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   		
      	$asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}	
              try{
              	if($roleName=='client')
      	{
           	        $tr = DB::table('trips')
                 		->join('company','company.id','=','trips.companyId')->where('company.userID',$userID)->where('trips.id',$id)
                 		->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->get();
           	        $TripsHistory = DB::table('trips')
                 		->join('triphistory','trips.id','=','triphistory.tripId')
                 		->join('company','company.id','=','trips.companyId')->where('trips.id',$id)->where('company.userID',$userID)
                 		->select('triphistory.*')->get();
 	           $driver1 = DB::table('trips')
           					->join('drivers','trips.assignedDriver','=','drivers.id')
           					->join('company','company.id','=','drivers.companyID')->where('trips.id',$id)->where('company.userID',$userID)
                 			->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')
           					->get();
           		$vehicles1=DB::table('trips')
           					->join('vehicles','trips.assignedVehicle','=','vehicles.id')
           					->join('company','company.id','=','vehicles.companyID')->where('trips.id',$id)->where('company.userID',$userID)
                 			->select('vehicles.id','vehicles.firstName')
           					->get();
         }
         else
         {
                  $tr = DB::table('trips')
                 		->join('branch','branch.id','=','trips.branchId')->where('branch.userID',$userID)
                 		->select('trips.id','trips.name','trips.startLocation','trips.startDate','trips.status','trips.companyId','trips.branchId')->where('trips.id',$id)->get();
           	        $TripsHistory = DB::table('trips')
                 		->join('triphistory','trips.id','=','triphistory.tripId')
                 		->join('branch','branch.id','=','trips.branchId')->where('branch.userID',$userID)
                 		->select('triphistory.*')->where('trips.id',$id)->get();
 	           $driver1 = DB::table('trips')
           					->join('drivers','trips.assignedDriver','=','drivers.id')
           					->join('branch','branch.id','=','drivers.branchID')->where('branch.userId',$userID)
                 			->select('drivers.id','drivers.firstName','drivers.lastName','drivers.middleName')->where('trips.id',$id)
           					->get();
           		$vehicles1=DB::table('trips')
           					->join('vehicles','trips.assignedVehicle','=','vehicles.id')
           					->join('branch','branch.id','=','vehicles.branchID')->where('trips.id',$id)->where('branch.userId',$userID)
                 			->select('vehicles.id','vehicles.firstName')
           					->get();
         }	
         	}
     	   catch(Exception $e)	
        	{
            echo $e;
        	}
      		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' =>[
                  		'trips'=>$tr,
                  		'TripsHistory'=>$TripsHistory,
                  		'driver'=>$driver1,
                  		'vehicles'=>$vehicles1
                  			],
                      ],   ]);
                 	 }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
public function putTriphistory(Request $request,$id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   	$asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}	
        try{
          $this->validate($request, [
                  'type'=>'required',
                  'location'=>'required',
                  'image'=>'required',
                  'reference'=>'required',
                    
                   ]); 
          }
          catch (ValidationException $e) 
              {
             return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>true,
                  'message' =>$e->getResponse()
                      ],
                ],211);
              }
      
              try{
       		        $expanse =TripsHistory::where('tripId',$id)->update([
       		     									'type'=>$request->get('type'),
								                 	'location'=>$request->get('location'),
								                   'image'=>$request->get('image'),
								            		'reference'=>$request->get('reference'),
								                   'tripId'=>$id,
								               ]);
    			     }
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Trips History  has been updated' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }

public function deleteTrips($id)
    {
          $user = JWTAuth::parseToken()->authenticate();
          $userID=$user->id;
          $user = new User();
          $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
     if($roleName=='client' OR $roleName=='branch')  
      {   	$asign11=Trip::find($id);
              if (!$asign11	) {
              return new JsonResponse([
               	    'apiResponse' =>[
               	    'error'=>false,
               	    'message' => 'Trips does not found' 
               	           ],   ]);              
             		}	
        
              try{
       				$deleteTrips=DB::table('triphistory')
                 		->join('trips','trips.id','=','triphistory.tripId')
                 		->join('company','company.id','=','trips.companyId')->where('trips.id',$id)->where('company.userID',$userID)
                 		->delete();
       			     }
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		  return new JsonResponse([
                  'apiResponse' =>[
                  'message' => 'TripsHistory has been deleted'
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not do somthing' 
                        ],   ]);
        }
    }
                  
}
