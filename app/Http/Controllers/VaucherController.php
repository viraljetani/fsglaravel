<?php

namespace App\Http\Controllers;
use App\User;
use App\Vaucher;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;

class VaucherController extends Controller
{
     public function postVaucher(Request $request)
   {
         	   	$user = JWTAuth::parseToken()->authenticate();
      		 	$id=$user->id;
      		  	$obj = new User();
                $role = $obj->getRole($id);
                 foreach ($role as $role1)
                   {
                      $roleName=$role1->name;
                    }
        if($roleName=='admin')  
        {
              	try{
 		  	           $this->validate($request, ['vaucherName'=>'required',
                                              ]);
 		  	    }
 		  	     catch (ValidationException $e) 
    	        {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' =>$e->getResponse()  
                            ],
                         ], 211);
        	    }
    	       try{
	       		        $expanse =Vaucher::create(['vaucherName'=> $request->get('vaucherName'),
                                                'amount'=> $request->get('amount')]);
    			}
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		      return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'The Vaucher has been created' 
            					  ],	 ]);
				 }
      else
        		{
	        		return new JsonResponse([
	              		'apiResponse' =>[
	                	'error'=>false,
	                 	'message' => 'Vaucher can not inserted ' 
	            					  ],	 ]);
            	}
		}
   public function getVauchers()
     {
      		$data = JWTAuth::parseToken()->authenticate();
      		$userID=$data->id;
      		$obj = new Vaucher();
      		$Vaucher = $obj->vaucherDetail();
    	   	     return new JsonResponse([
                    'apiResponse' =>[
                    'error'=>false,                
                    'message' => 'authenticated_user'
                					],
                   'userProfile' => $data,
                   'Vaucher Detail'=>$Vaucher
    	 					    ]); 
     }
 public function getVaucher($id)
     {
    				$data = JWTAuth::parseToken()->authenticate();
    				$userID=$data->id;
		  	  	    $Vaucher=Vaucher::find($id);
    	 		 	if (!$Vaucher) 
            		{
    					return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Vaucher does not found' 
                					  ],	 ]);		       		
           			 }
          		  else{
                  		$obj = new Vaucher();
            					$vaucherDetail = $obj->vaucherDetail()->where('id',$id);
      	   	     			return new JsonResponse([
                      		'apiResponse' =>[
                        		'error'=>false,                
                        		'message' => 'authenticated_user'
                    						],
                     			'userProfile' => $data,
                     			'Vaucher Detail'=>$vaucherDetail
        	 					    ]); 
          			}
  	}
public function deleteVaucher($id)
    {	
      			$data = JWTAuth::parseToken()->authenticate();
      			$userID=$data->id;
         		$user = new User();
	           	$role = $user->getRole($userID);
              	  foreach ($role as $role1)
             	 		{
                  	$roleName=$role1->name;
              	  }
     	if($roleName=='admin')  
      	{
				  	$Vaucher=Vaucher::find($id);
    	 		 	if (!$Vaucher) 
              			{
      					return new JsonResponse([
                    		'apiResponse' =>[
                      		'error'=>false,
                       		'message' => 'Vaucher does not found' 
                  					  ],	 ]);		       		
              			}
			 		$Vaucher =DB::table('vaucher')->where('id',$id)->delete();
   				  return new JsonResponse([
                'apiResponse' =>[
                'error'=>false,                	
                'message' => 'Vaucher Detail id  deleted' 
                ],
            	]);		
   		 }
   		else
        {
        			return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'you can not Delete' 
            			  ],	 ]);
           }
     }
    public function putVaucher(Request $request,$id)
   {

   	   	   $data = JWTAuth::parseToken()->authenticate();
   	   	   $userID = $data->id;
  	   	   $user = new User();
         	$role = $user->getRole($userID);
          	  foreach ($role as $role1)
         	 		{
              	$roleName=$role1->name;
         		  }
   		 	   
 	 if($roleName=='admin')  
       	{
				  	$Vaucher=Vaucher::find($id);
    	 		 	if (!$Vaucher) {
    	 				return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'Vaucher does not found' 
                					  ],	 ]);		       		
     						 }
    		try{
    			    $this->validate($request, ['vaucherName'=>'required',
                                              ]);
  				}												
 		  	     catch (ValidationException $e) 
    	        {
                return new JsonResponse([
                'apiResponse' =>[
                'error'=>true,
                'message' =>$e->getResponse()  
                            ],
                          ], 211);
        	    }
           try{
       		        DB::table('vaucher')->where('id',$id)->update([	          		
              				'vaucherName'=> $request->get('vaucherName'),
                       'amount'=> $request->get('amount')]);
      			}
      		  	 catch(Exception $e)
      					   {
						       return $e;
    					     }
       		      return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'The Vaucher has been Updated' 
            					  ],	 ]);
				 }
      else
        		{
     					return new JsonResponse([
	              		'apiResponse' =>[
	                	'error'=>false,
	                 	'message' => 'Vaucher can not Updated ' 
	            					  ],	 ]);
            	}
		}
}
