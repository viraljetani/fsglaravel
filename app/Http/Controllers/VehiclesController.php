<?php

namespace App\Http\Controllers;
use App\Company;
use App\Branch;
use App\Vehicles;
use App\User;
use App\Driver;
use DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Hash;
use Auth;
class VehiclesController extends Controller
{
  public function postVehicles(Request $request)
   {
   		 $user = JWTAuth::parseToken()->authenticate();
    		$userID=$user->id;
    		$obj = new Company();
	      $obj1=new Branch();
	     	$companyID= $obj->CompaniesDetail($userID);
      	$Branch= $obj1->BranchesDetail($userID);
        $user = new User();
         $role = $user->getRole($userID);
                foreach ($role as $role1)
               {
                  $roleName=$role1->name;
                }
  if($roleName=='client' OR $roleName=='branch')  
      {
        if($roleName=='client')
          {

          $companyID= $obj->CompaniesDetail($userID);
              foreach ($companyID as $compID)
                 {
                   $compID1=$compID->id;
                  $brnID='0';
             
                  } 
          }

          else
          {
          $Branch= $obj1->BranchesDetail($userID);
                 foreach ($Branch as $branch)
                 {
                   $brnID=$branch->id;
                   $compID1=$branch->companyId;
              
                  } 
           }   
       
      	try{	
   				  $this->validate($request, [
                              'firstName'=>'required', 
    	           							'typeOfVehicle'=>'required', 
    	           							'brand'=>'required', 
    	           							'model'=>'required', 
    	           							'version'=>'required', 
    	           							'kind'=>'required', 
    	           							'modelYear'=>'required',
    	           							'typeOfInComp'=>'required',
    	           							'typeOfPolicy'=>'required',
    	           							'policyNo'=>'required', 
    	           							'salesRepName'=>'required',
    	           							'costPolicy'=>'required', 
    	           							'policyEndDate'=>'required|date_format:d/m/Y',
    	           							'phone'=>'required', 
    	           							'initialCost'=>'required', 
    	           							'newPurchase'=>'required',
    	           							'acturePrice'=>'required',
    	           							'endDateOfGuarantee'=>'required|date_format:d/m/Y',
    	           							'vehiclPhoto'=>'required', 
    	           							'photoLicensePlat'=>'required',
    	           							'invoicesPhoto'=>'required', 
    	           							'photoOfCard'=>'required',
    	           							'initialOdometer'=>'required',
    	           							'realOdometer'=>'required',
    	           							'litersFuelTank'=>'required',
    	           							'kilometersPerLiter'=>'required',
    	           							'generalConUnit'=>'required',
    	           							'observations'=>'required',
                              'userId'=>'required'
    	   	  							]);
   				  	}
   				  	       catch (ValidationException $e) 
					            {
					            return new JsonResponse([
				  	                'apiResponse' =>[
					                  'error'=>true,
					                  'message'=>$e->getResponse()    
                        					 ], ],211);
					            }
					     try{
               					 $vehicles =Vehicles::create([
	               					 			'firstName'=> $request->get('firstName'), 
	    	           							'typeOfVehicle'=> $request->get('typeOfVehicle'), 
	    	           							'brand'=> $request->get('brand'), 
	    	           							'model'=> $request->get('model'), 
	    	           							'version'=> $request->get('version'), 
	    	           							'kind'=> $request->get('kind'), 
	    	           							'modelYear'=> $request->get('modelYear'),
	    	           							'typeOfInComp'=> $request->get('typeOfInComp'),
	    	           							'typeOfPolicy'=> $request->get('typeOfPolicy'),
	    	           							'policyNo'=> $request->get('policyNo'), 
	    	           							'salesRepName'=> $request->get('salesRepName'),
	    	           							'costPolicy'=> $request->get('costPolicy'), 
	    	           							'policyEndDate'=> $request->get('policyEndDate'),
	    	           							'phone'=> $request->get('phone'), 
	    	           							'initialCost'=> $request->get('initialCost'), 
	    	           							'newPurchase'=> $request->get('newPurchase'),
	    	           							'acturePrice'=> $request->get('acturePrice'),
	    	           							'endDateOfGuarantee'=> $request->get('endDateOfGuarantee'), 
	    	           							'vehiclPhoto'=> $request->get('vehiclPhoto'), 
	    	           							'photoLicensePlat'=> $request->get('photoLicensePlat'),
	    	           							'invoicesPhoto'=> $request->get('invoicesPhoto'), 
	    	           							'photoOfCard'=> $request->get('photoOfCard'),
	    	           							'initialOdometer'=> $request->get('initialOdometer'),
	    	           							'realOdometer'=> $request->get('realOdometer'),
	    	           							'litersFuelTank'=> $request->get('litersFuelTank'),
	    	           							'kilometersPerLiter'=> $request->get('kilometersPerLiter'),
	    	           							'generalConUnit'=> $request->get('generalConUnit'),
	    	          							'observations'=> $request->get('observations'),
	    	         								'action'=>'0',
                                'userId'=> $request->get('userId'),
	           										'companyId'=>$compID1,
                   							 'branchId'=>$brnID
               					 					]);	
  							}
					catch(Exception $e)
					           {
							                return $e;
					            }
    		  return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Vehicles has been Created' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not register' 
                        ],   ]);
        }
      }
  public function getVihecles()
     {
            $data = JWTAuth::parseToken()->authenticate();
            $userID=$data->id;
            $obj = new Vehicles();
            $obj1 = new Company();
            $obj2 = new Branch();
            $vehicles=$obj->VehiclesDetail($userID);
            $comp = $obj1->CompaniesDetail($userID);
            $Branch= $obj2->BranchesDetail($userID);
               return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false
                              ],
                    'message' => 'authenticated_user',
                   'userProfile' => $data,
                   'vehiclesDetail'=>$vehicles,
                   'companyDetail'=>$comp,
                   'BranchesDetail'=>$Branch
                         ]); 
             }        
  public function getVihecle($id)
     {
    	   $vehi=Vehicles::find($id);
         		 if (!$vehi) {
    				return new JsonResponse([
                  		'apiResponse' =>[
                    	'error'=>false,
                     	'message' => 'vehicles  does not found' 
                					  ],	 ]);		       			 
              }
          else
          {
              $data = JWTAuth::parseToken()->authenticate();
              $userID=$data->id;
              $obj = new Vehicles();
              $obj1 = new Company();
              $obj2 = new Branch();
              $vehicles=$obj->VehiclesDetail($userID)->where('id',$id);
              $comp = $obj1->CompaniesDetail($userID);
              $Branch= $obj2->BranchesDetail($userID);
                   return new JsonResponse([
                            'apiResponse' =>[
                            'error'=>false
                                    ],
                             'message' => 'authenticated_user',
                             'userProfile' => $data,
                             'vehiclesDetail'=>$vehicles,
                             'companyDetail'=>$comp,
                              'BranchesDetail'=>$Branch
                         
                              ]); 
        }
    }
  public function deleteVehicles($id)
     	{
               $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                 $user = new User();
                 $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='client' OR $roleName=='branch')  
      {
             $vehi=Vehicles::find($id);
             if (!$vehi) {
            return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'vehicles  does not found' 
                            ],   ]);                 
              }
	          $comp_id =DB::table('vehicles')->where('id',$id)->delete();
	  	 		 return new JsonResponse([
              		'apiResponse' =>[
                	'error'=>false,
                 	'message' => 'vehicles  id  deleted' 
            					  ],	 ]);		
          }
      else
         {
               return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'you can not Delete' 
                            ],   ]);
         }
  }
  public function putVehicles(Request $request,$id)
   {
   		 $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $obj = new Company();
                $obj1=new Branch();
                $obj2 = new Vehicles();
                $user = new User();
               $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='client' OR $roleName=='branch')  
      {
             $vehi=Vehicles::find($id);
             if (!$vehi) {
            return new JsonResponse([
                      'apiResponse' =>[
                      'error'=>false,
                      'message' => 'vehicles  does not found' 
                            ],   ]);                 
              }
              
          $companyID= $obj->CompaniesDetail($userID);
          $Branch= $obj1->BranchesDetail($userID);
          $vehicles = $obj2->VehiclesDetail($userID)->where('id',$id);

               if($roleName=='client')
          {

          $companyID= $obj->CompaniesDetail($userID);
              foreach ($companyID as $compID)
                 {
                   $compID1=$compID->id;
                  $brnID='0';
             
                  } 
          }

          else
          {
          $Branch= $obj1->BranchesDetail($userID);
                 foreach ($Branch as $branch)
                 {
                   $brnID=$branch->id;
                   $compID1=$branch->companyId;
              
                  } 
           }   
  	   try{
            $this->validate($request, [
                             'firstName'=>'required', 
    	        							'typeOfVehicle'=>'required', 
    	           							'brand'=>'required', 
    	           							'model'=>'required', 
    	           							'version'=>'required', 
    	           							'kind'=>'required', 
    	           							'modelYear'=>'required',
    	           							'typeOfInComp'=>'required',
    	           							'typeOfPolicy'=>'required',
    	           							'policyNo'=>'required', 
    	           							'salesRepName'=>'required',
    	           							'costPolicy'=>'required', 
    	           							'policyEndDate'=>'required|date_format:d/m/Y',
    	           							'phone'=>'required', 
    	           							'initialCost'=>'required', 
    	           							'newPurchase'=>'required',
    	           							'acturePrice'=>'required',
    	           							'endDateOfGuarantee'=>'required|date_format:d/m/Y',
    	           							'vehiclPhoto'=>'required', 
    	           							'photoLicensePlat'=>'required',
    	           							'invoicesPhoto'=>'required', 
    	           							'photoOfCard'=>'required',
    	           							'initialOdometer'=>'required',
    	           							'realOdometer'=>'required',
    	           							'litersFuelTank'=>'required',
    	           							'kilometersPerLiter'=>'required',
    	           							'generalConUnit'=>'required',
    	           							'observations'=>'required',
    	   	  							]);
   	  	}
   				  	  catch (ValidationException $e) 
					        {
					           return new JsonResponse([
					                'apiResponse' =>[
					                'error'=>true,
					                  'message'=>$e->getResponse()    
                        					 ], ],211);
					          }
			  		  DB::table('vehicles')->where('id',$id)->update([ 
         		  						           'firstName'=> $request->get('firstName'), 
                                'typeOfVehicle'=> $request->get('typeOfVehicle'), 
                                'brand'=> $request->get('brand'), 
                                'model'=> $request->get('model'), 
                                'version'=> $request->get('version'), 
                                'kind'=> $request->get('kind'), 
                                'modelYear'=> $request->get('modelYear'),
                                'typeOfInComp'=> $request->get('typeOfInComp'),
                                'typeOfPolicy'=> $request->get('typeOfPolicy'),
                                'policyNo'=> $request->get('policyNo'), 
                                'salesRepName'=> $request->get('salesRepName'),
                                'costPolicy'=> $request->get('costPolicy'), 
                                'policyEndDate'=> $request->get('policyEndDate'),
                                'phone'=> $request->get('phone'), 
                                'initialCost'=> $request->get('initialCost'), 
                                'newPurchase'=> $request->get('newPurchase'),
                                'acturePrice'=> $request->get('acturePrice'),
                                'endDateOfGuarantee'=> $request->get('endDateOfGuarantee'), 
                                'vehiclPhoto'=> $request->get('vehiclPhoto'), 
                                'photoLicensePlat'=> $request->get('photoLicensePlat'),
                                'invoicesPhoto'=> $request->get('invoicesPhoto'), 
                                'photoOfCard'=> $request->get('photoOfCard'),
                                'initialOdometer'=> $request->get('initialOdometer'),
                                'realOdometer'=> $request->get('realOdometer'),
                                'litersFuelTank'=> $request->get('litersFuelTank'),
                                'kilometersPerLiter'=> $request->get('kilometersPerLiter'),
                                'generalConUnit'=> $request->get('generalConUnit'),
                                'observations'=> $request->get('observations'),
                                'action'=>'0',
                                'userId'=>$userID,
                                'companyId'=>$compID1,
                                'branchId'=>$brnID
                                  ]); 
         		         return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Vehicles has been Updated' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not Update' 
                        ],   ]);
        }
    }
  public function asignVehicles()
  {
     $data = JWTAuth::parseToken()->authenticate();
                $userID=$data->id;
                $user = new User();
               $role = $user->getRole($userID);
                    foreach ($role as $role1)
                        {
                          $roleName=$role1->name;
                        }
        if($roleName=='client' OR $roleName=='branch')  
      {
          //action o for deActivate And 1 for activate
        
        $vehicles=DB::table("vehicles")->where('action','=','0')->select('id')->get();
          
        $driver=DB::table("drivers")->where('action','=','0')->select('id')->get();
               try{
                  $vehiclesAsign = DB::table('asignvehicles')->insert(array('driverId','=',$vehicles,'vehiclesId','=',$driver));
                  $update=DB::table('drivers')->join('vehicles','vehicles.action','=','drivers.action')->update('drivers.action','=','1','vehicles.action','=','1');
                 }
               
          catch(Exception $e)
                     {
                              return $e;
                      }
          return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'The Vehicles has been not asign' 
                        ],   ]);
        }
        else
        {
              return new JsonResponse([
                  'apiResponse' =>[
                  'error'=>false,
                  'message' => 'you can not asign' 
                        ],   ]);
        }
      }
 }
  
