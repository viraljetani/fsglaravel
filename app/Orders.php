<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Orders extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table='orders';

       protected $fillable =[
       						'name',
       						'type',
       						'dropLocation',
       						'customerID',
       						'expectedDeliveryDate',
       					
       							];

     public function OrdersDetail($id)
     {
            $Orders=DB::table('customer')->join('orders','customer.id','=','orders.customerId')
                                        ->join('users','customer.userId','=','users.id')->select('customer.*','orders.*')->get(); 
                                        
                                        // ->join('company','company.id','=','customer.companyId')->where('company.userId',$id)->select('company.*','customer.*','orders.*')->get(); 

                                          
    
        return $Orders;
    }


}
