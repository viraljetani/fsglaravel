<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShowParkingModel extends Model
{
 	protected $table = 'tbl_parking';
 	protected $fillable = ['name','city','location'];
}
