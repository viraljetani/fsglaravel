<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Trip extends Model  implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table='trips';

       protected $fillable =[
       						'name',
       						'startLocation',
       						'startDate',
       						'status',
       						'note',
       						'assignedDriver',
       						'assignedVehicle',
       						'companyId',
       						'branchId',
       						'created_at',
       						 'updated_at'
       							];


}
