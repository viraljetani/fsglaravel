<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class TripsHistory extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table='triphistory';

       protected $fillable =[
       						
       						'type',
       						'location',
       						'image',
       						'reference',
       						'tripId',
       						'created_at',
       						 'updated_at'
       							];


}
