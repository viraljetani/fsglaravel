<?php



namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Vaucher extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
       use Authenticatable, Authorizable;
       protected $table='vaucher';
        protected $fillable = ['vaucherName'];	

     public function vaucherDetail()
     {
            $vaucher=DB::table('vaucher')->select('*')->get(); 
            return $vaucher;
    }
}
