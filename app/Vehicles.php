<?php


namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth;
use DB;

class Vehicles extends Model implements
    AuthenticatableContract,
    AuthorizableContract
{
    use Authenticatable, Authorizable;

    	    protected $table='vehicles';

    	           protected $fillable =[
    	           							'firstName', 
    	           							'typeOfVehicle', 
    	           							'brand', 
    	           							'model', 
    	           							'version', 
    	           							'kind', 
    	           							'modelYear',
    	           							'typeOfInComp',
    	           							'typeOfPolicy',
    	           							'policyNo', 
    	           							'salesRepName',
    	           							'costPolicy', 
    	           							'policyEndDate',
    	           							'phone', 
    	           							'initialCost', 
    	           							'newPurchase',
    	           							'acturePrice',
    	           							'endDateOfGuarantee', 
    	           							'vehiclPhoto', 
    	           							'photoLicensePlat',
    	           							'invoicesPhoto', 
    	           							'photoOfCard',
    	           							'initialOdometer',
    	           							'realOdometer',
    	           							'litersFuelTank',
    	           							'kilometersPerLiter',
    	           							'generalConUnit',
    	           							'observations',
                                            'action',
    	           							'userId',
    	           							'companyId',
    	           							'branchId'
    	           								];

         public function VehiclesDetail($id)
     {
            $vehicles=DB::table('vehicles')->where('userID',$id)->select('*')->get(); 
    
        return $vehicles;
    }
}
