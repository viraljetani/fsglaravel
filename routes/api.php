<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = $app->make(Dingo\Api\Routing\Router::class);
$app->routeMiddleware([
           'cors' => '/lumen/app/Http/Middleware/CORSMiddleware',
              ]);
              
$api->version('v1', function ($api) {

       $api->post('/auth/login', ['as' => 'api.auth.login','uses' => 'App\Http\Controllers\Auth\AuthController@postLogin',]);

     $api->post('/auth/register', ['as' => 'api.auth.register','uses' => 'App\Http\Controllers\Auth\AuthController@postUser', ]);
    
   $api->put('/auth/changeuser', ['as' => 'api.auth.change', 'uses' => 'App\Http\Controllers\Auth\AuthController@userChange',]);

        //Create a Company 
              $api->post('/auth/companyRegister', ['as' => 'api.auth.companyregister','uses' => 'App\Http\Controllers\CompanyController@postCompany',]);
         
                $api->put('/auth/companyUpdate/{id}', ['as' => 'api.auth.companyUpdate','uses' => 'App\Http\Controllers\CompanyController@putCompany', ]);

         //create a Branch
                $api->post('/auth/branchRegister', ['as' => 'api.auth.branchregister','uses' => 'App\Http\Controllers\BranchController@postBranch',]);

                $api->put('/auth/branchUpdate/{id}', ['as' => 'api.auth.branchUpdate','uses' => 'App\Http\Controllers\BranchController@putBranch',]);
         
         //Create a Driver
                $api->post('/auth/driverRegister', ['as' => 'api.auth.driverregister','uses' => 'App\Http\Controllers\DriverController@postDriver', ]);

                $api->put('/auth/driverUpdate/{id}', ['as' => 'api.auth.driverUpdate','uses' => 'App\Http\Controllers\DriverController@putDriver',]);
//Create a Vehicles
                $api->post('/auth/vehiclesRegister', ['as' => 'api.auth.vehiclesregister','uses' => 'App\Http\Controllers\VehiclesController@postVehicles', ]);

                $api->put('/auth/vehiclesUpdate/{id}', ['as' => 'api.auth.vehiclesUpdate','uses' => 'App\Http\Controllers\VehiclesController@putVehicles',]);
    //asign a Vehicles
                // $api->post('/auth/asignVehicle', ['as' => 'api.auth.asignVehicle','uses' => 'App\Http\Controllers\TripController@asignVehicles', ]);  
                
                 // $api->put('/auth/asignVehicle/{id}', ['as' => 'api.auth.updateAsignVehicles','uses' => 'App\Http\Controllers\TripController@updateAsignVehicles', ]);
    //expanse management
              $api->post('/auth/postExpanse', ['as' => 'api.auth.postExpanse','uses' => 'App\Http\Controllers\ExpanseController@postExpanse', ]);
             
                $api->put('/auth/putExpanse/{id}', ['as' => 'api.auth.putExpanse','uses' => 'App\Http\Controllers\ExpanseController@putExpanse', ]);
    //vaucher
             $api->post('/auth/postVaucher', ['as' => 'api.auth.postVaucher','uses' => 'App\Http\Controllers\VaucherController@postVaucher', ]);
             
                $api->put('/auth/putVaucher/{id}', ['as' => 'api.auth.putVaucher','uses' => 'App\Http\Controllers\VaucherController@putVaucher', ]); 
 //asign VAUCHER
                 $api->post('/auth/asignVaucher', ['as' => 'api.auth.asignVaucher','uses' => 'App\Http\Controllers\ExpanseController@asignVaucher',]);
          
             $api->put('/auth/asignVaucher/{id}', ['as' => 'api.auth.putAsignVaucher','uses' => 'App\Http\Controllers\ExpanseController@putAsignVaucher',]);
  //Trips 
              $api->post('/auth/Trips', ['as' => 'api.auth.postTrips','uses' => 'App\Http\Controllers\TripController@postTrips',]);
          
             $api->put('/auth/Trips/{id}', ['as' => 'api.auth.postTrips','uses' => 'App\Http\Controllers\TripController@putTrips',]);
  //Trips History
              $api->post('/auth/Triphistory/{id}', ['as' => 'api.auth.Triphistory','uses' => 'App\Http\Controllers\TripsHistoryController@postTriphistory',]);
          
             $api->put('/auth/Triphistory/{id}', ['as' => 'api.auth.Triphistory','uses' => 'App\Http\Controllers\TripsHistoryController@putTriphistory',]);
    //orders
               $api->post('/auth/orders', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\OrdersController@postOrders',]);
          
             $api->put('/auth/orders/{id}', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\OrdersController@putOrders',]);
      //orders
               $api->post('/auth/customer', ['as' => 'api.auth.Customer','uses' => 'App\Http\Controllers\CustomerController@postCustomer',]);
          
             $api->put('/auth/customer/{id}', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\CustomerController@putCustomer',]);
    $api->group([
            'middleware' => 'api.auth',
              ], function ($api) 
                {
                   $api->get('/', ['uses' => 'App\Http\Controllers\APIController@getIndex','as' => 'api.index']);

                  $api->get('/auth/user', ['uses' => 'App\Http\Controllers\Auth\AuthController@getUser','as' => 'api.auth.user']);
                  
                  $api->patch('/auth/refresh', ['uses' => 'App\Http\Controllers\Auth\AuthController@patchRefresh','as' => 'api.auth.refresh']);
    
                    $api->delete('/auth/invalidate', ['uses' => 'App\Http\Controllers\Auth\AuthController@deleteInvalidate','as' => 'api.auth.invalidate']);
        //Companies
                $api->get('/auth/companies', ['as' => 'api.auth.company', 'uses' => 'App\Http\Controllers\CompanyController@getCompanies',]);   
                
                $api->get('/auth/companies/{id}', ['as' => 'api.auth.company', 'uses' => 'App\Http\Controllers\CompanyController@getCompany',]);

                $api->delete('/auth/deleteCompany/{id}', ['as' => 'api.auth.deletecompany','uses' =>'App\Http\Controllers\CompanyController@deleteCompany',]);
//Branch
                   $api->get('/auth/branch', ['as' => 'api.auth.branch', 'uses' => 'App\Http\Controllers\BranchController@getBranches',]);
                    
                    $api->get('/auth/branch/{id}', ['as' => 'api.auth.branch', 'uses' => 'App\Http\Controllers\BranchController@getBranch',]);


                    $api->delete('/auth/deleteBranch/{id}', ['as' => 'api.auth.deletebranch','uses' =>'App\Http\Controllers\BranchController@deleteBranch', ]);
     //Driver 
                    $api->get('/auth/drivers', ['as' => 'api.auth.driver','uses' => 'App\Http\Controllers\DriverController@getDrivers',]);

                    $api->get('/auth/drivers/{id}', ['as' => 'api.auth.driver','uses' => 'App\Http\Controllers\DriverController@getDriver',]);

                        $api->delete('/auth/drivers/{id}', ['as' => 'api.auth.deletedriver','uses' => 'App\Http\Controllers\DriverController@deleteDriver',]);

     // Vehicles
                        $api->get('/auth/vehicles', ['as' => 'api.auth.vehicles','uses' => 'App\Http\Controllers\VehiclesController@getVihecles',]);

                         $api->get('/auth/vehicles/{id}', ['as' => 'api.auth.vehicles','uses' => 'App\Http\Controllers\VehiclesController@getVihecle',]);

                          $api->delete('/auth/vehicles/{id}', ['as' => 'api.auth.deletevehicles','uses' => 'App\Http\Controllers\VehiclesController@deleteVehicles',]);
    //asign
                          // $api->get('/auth/asignVehicle', ['as' => 'api.auth.asignVehicle','uses' => 'App\Http\Controllers\TripController@getVehiclesDriver', ]); 

                           // $api->delete('/auth/asignVehicle/{id}', ['as' => 'api.auth.deleteAsignVehicles','uses' => 'App\Http\Controllers\TripController@deleteAsignVehicles', ]);
        //expanse

                          $api->get('/auth/expanses', ['as' => 'api.auth.expanse','uses' => 'App\Http\Controllers\ExpanseController@getExpanses',]);

                         $api->get('/auth/expanses/{id}', ['as' => 'api.auth.expanse','uses' => 'App\Http\Controllers\ExpanseController@getExpanse',]);

                          $api->delete('/auth/expanses/{id}', ['as' => 'api.auth.deleteExpanse','uses' => 'App\Http\Controllers\ExpanseController@deleteExpanse',]);
  //  Vaucher
                          $api->get('/auth/Vaucher', ['as' => 'api.auth.getVauchers','uses' => 'App\Http\Controllers\VaucherController@getVauchers',]);

                         $api->get('/auth/Vaucher/{id}', ['as' => 'api.auth.getVaucher','uses' => 'App\Http\Controllers\VaucherController@getVaucher',]);

                          $api->delete('/auth/Vaucher/{id}', ['as' => 'api.auth.deleteVaucher','uses' => 'App\Http\Controllers\VaucherController@deleteVaucher',]);
    //asign VAUCHER
                       $api->get('/auth/VaucherInfo/{id}', ['as' => 'api.auth.getVaucherInfo','uses' => 'App\Http\Controllers\ExpanseController@getVaucherInfo',]);
                      $api->delete('/auth/asignVaucher/{id}', ['as' => 'api.auth.deleteAsignVaucher','uses' => 'App\Http\Controllers\ExpanseController@deleteAsignVaucher',]);
//asign trips
                     $api->get('/auth/asignVehicle', ['as' => 'api.auth.asignVehicle','uses' => 'App\Http\Controllers\TripController@getVehiclesDriver', ]); 

                       $api->get('/auth/Trips', ['as' => 'api.auth.getTrips','uses' => 'App\Http\Controllers\TripController@getTrips',]);

                       $api->get('/auth/Trips/{id}', ['as' => 'api.auth.getTrip','uses' => 'App\Http\Controllers\TripController@getTrip',]);
                       
                      $api->delete('/auth/Trips/{id}', ['as' => 'api.auth.deleteTrips','uses' => 'App\Http\Controllers\TripController@deleteTrips',]);
//TRips History
                   $api->get('/auth/Triphistory', ['as' => 'api.auth.Triphistory','uses' => 'App\Http\Controllers\TripsHistoryController@getTriphistories',]);

                       $api->get('/auth/Triphistory/{id}', ['as' => 'api.auth.Triphistory','uses' => 'App\Http\Controllers\TripsHistoryController@getTriphistory',]);
                       
                      $api->delete('/auth/Triphistory/{id}', ['as' => 'api.auth.Triphistory','uses' => 'App\Http\Controllers\TripsHistoryController@deleteTrips',]);
//get services 
                      $api->get('/auth/services', ['as' => 'api.auth.services','uses' => 'App\Http\Controllers\ServicesController@getservices',]);
  //orders OrdersController

                        $api->get('/auth/orders', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\OrdersController@getOrders',]);

                       $api->get('/auth/orders/{id}', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\OrdersController@getOrder',]);
                       
                      $api->delete('/auth/orders/{id}', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\OrdersController@deleteOrders',]);
          //CUSTOMER
                      $api->get('/auth/customer', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\CustomerController@getCustomers',]);

                       $api->get('/auth/customer/{id}', ['as' => 'api.auth.orders','uses' => 'App\Http\Controllers\CustomerController@getCustomer',]);
                       
                      $api->delete('/auth/customer/{id}', ['as' => 'api.auth.Customer','uses' => 'App\Http\Controllers\CustomerController@deleteCustomer',]);  
    });
});
